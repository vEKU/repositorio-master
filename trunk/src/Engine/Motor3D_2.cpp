

// INPUT DEFINITIONS

namespace InputDefinitions
{
	enum MouseButton {
		LEFT, MIDDLE, RIGHT
	};
	enum MouseAxis {
		MOUSE_X, MOUSE_Y, DX, DY, DZ
	};
	enum GamepadButton {
		A, B, X, Y, START, BACK, LEFT_THUMB, RIGHT_THUMB, LB, RB, DPAD_UP, DPAD_LEFT, DPAD_RIGHT, DPAD_DOWN
	};
	enum GamepadAxis {
		LEFT_THUMB_X, LEFT_THUMB_Y, RIGHT_THUMB_X, RIGHT_THUMB_Y, LEFT_TRIGGER, RIGHT_TRIGGER
	};

	inline MouseButton GetMouseButtonFromString(const char* str, MouseButton defaultValue = (MouseButton)-1)
	{
		if (str == nullptr)
			return defaultValue;
		else if (strcmp(str, "LEFT") == 0)
			return LEFT;
		else if (strcmp(str, "MIDDLE") == 0)
			return MIDDLE;
		else if (strcmp(str, "RIGHT") == 0)
			return RIGHT;
		else
			return (MouseButton)-1;
	}

	inline MouseAxis GetMouseAxisFromString(const char* str, MouseAxis defaultValue = (MouseAxis)-1)
	{
		if (str == nullptr)
			return defaultValue;
		else if (strcmp(str, "MOUSE_X") == 0)
			return MOUSE_X;
		else if (strcmp(str, "MOUSE_Y") == 0)
			return MOUSE_Y;
		else if (strcmp(str, "DX") == 0)
			return DX;
		else if (strcmp(str, "DY") == 0)
			return DY;
		else if (strcmp(str, "DZ") == 0)
			return DZ;
		else
			return (MouseAxis)-1;
	}

	inline GamepadButton GetGamepadButtonFromString(const char* str, GamepadButton defaultValue = (GamepadButton)-1)
	{
		// TODO
		return (GamepadButton)-1;
	}

	inline GamepadAxis GetGamepadAxisFromString(const char* str, GamepadAxis defaultValue = (GamepadAxis)-1)
	{
		// TODO
		return (GamepadAxis)-1;
	}
}

// INPUT MANAGER HEADER

#include "InputDefinitions.h"

class CInputManager
{
public:

	CInputManager(HWND hWnd);
	virtual ~CInputManager();

	bool HandleKeyboard(const MSG& msg);
	bool HandleMouse(const MSG& msg);

	void PreUpdate(bool isWindowActive);
	void PostUpdate();

	// ----

	bool IsKeyPressed(unsigned char KeyCode) const { return m_KeyboardState.raw[KeyCode]; }
	bool KeyBecomesPressed(unsigned char KeyCode) const { return m_KeyboardState.raw[KeyCode] && !m_PreviousKeyboardState.raw[KeyCode]; }
	bool KeyBecomesReleased(unsigned char KeyCode) const { return !m_KeyboardState.raw[KeyCode] && m_PreviousKeyboardState.raw[KeyCode]; }

	bool IsEscapePressed(unsigned char KeyCode) const { return m_KeyboardState.escape; }
	bool EscapeBecomesPressed(unsigned char KeyCode) const { return m_KeyboardState.escape && !m_PreviousKeyboardState.escape; }
	bool EscapeBecomesReleased(unsigned char KeyCode) const { return !m_KeyboardState.escape && m_PreviousKeyboardState.escape; }

	bool IsSpacePressed(unsigned char KeyCode) const { return m_KeyboardState.space; }
	bool SpaceBecomesPressed(unsigned char KeyCode) const { return m_KeyboardState.space && !m_PreviousKeyboardState.space; }
	bool SpaceBecomesReleased(unsigned char KeyCode) const { return !m_KeyboardState.space && m_PreviousKeyboardState.space; }

	// ----

	int GetMouseMovementX() const { return m_MouseMovementX; }
	int GetMouseMovementY() const { return m_MouseMovementY; }
	int GetMouseMovementZ() const { return m_MouseMovementZ; }

	int GetMouseX() const { return m_MouseX; }
	int GetMouseY() const { return m_MouseY; }

	int GetMouseAxis(InputDefinitions::MouseAxis axis) const;

	bool IsLeftMouseButtonPressed() const { return m_ButtonLeft; }
	bool IsMiddleMouseButtonPressed() const { return m_ButtonMiddle; }
	bool IsRightMouseButtonPressed() const { return m_ButtonRight; }

	bool LeftMouseButtonBecomesPressed() const { return m_ButtonLeft && !m_PreviousButtonLeft; }
	bool MiddleMouseButtonBecomesPressed() const { return m_ButtonMiddle && !m_PreviousButtonMiddle; }
	bool RightMouseButtonBecomesPressed() const { return m_ButtonRight && !m_PreviousButtonRight; }

	bool LeftMouseButtonBecomesReleased() const { return !m_ButtonLeft && m_PreviousButtonLeft; }
	bool MiddleMouseButtonBecomesReleased() const { return !m_ButtonMiddle && m_PreviousButtonMiddle; }
	bool RightMouseButtonBecomesReleased() const { return !m_ButtonRight && m_PreviousButtonRight; }


	bool IsMouseButtonPressed(InputDefinitions::MouseButton button) const;
	bool MouseButtonBecomesPressed(InputDefinitions::MouseButton button) const;
	bool MouseButtonBecomesReleased(InputDefinitions::MouseButton button) const;

	// ----
	bool HasGamepad() const { return m_HasGamepad; }

	float GetGamepadLeftThumbX() const { return m_GamepadStickLeftX; }
	float GetGamepadLeftThumbY() const { return m_GamepadStickLeftY; }

	bool IsGamepadAPressed() const { return m_GamepadA; }

private:

	bool UpdateKeyboard();
	bool UpdateMouse();

	struct KeyboardData
	{
		bool raw[256];
		bool escape, space;
		bool numpad[10];
		bool fkey[24];
		bool left, right, up, down;

		void SetKey(unsigned char key, bool state);

	} m_PreviousKeyboardState, m_KeyboardState;

	bool m_HighPrecisionMouseRegistered;

	int									m_MouseMovementX;
	int									m_MouseMovementY;
	int									m_MouseMovementZ;
	int									m_MouseX;
	int									m_MouseY;
	bool								m_ButtonRight, m_PreviousButtonRight;
	bool								m_ButtonLeft, m_PreviousButtonLeft;
	bool								m_ButtonMiddle, m_PreviousButtonMiddle;

	bool								m_HasGamepad;
	float								m_GamepadStickLeftX, m_GamepadStickLeftY;
	bool								m_GamepadA, m_PreviousGamepadA;
};

inline int CInputManager::GetMouseAxis(InputDefinitions::MouseAxis axis) const {
	switch (axis)
	{
	case InputDefinitions::MOUSE_X:
		return m_MouseX;
	case InputDefinitions::MOUSE_Y:
		return m_MouseY;
	case InputDefinitions::DX:
		return m_MouseMovementX;
	case InputDefinitions::DY:
		return m_MouseMovementY;
	case InputDefinitions::DZ:
		return m_MouseMovementZ;
	default:
		assert(false);
		return 0;
	}
}

// INPUT MANAGER CPP


void SetupHighprecisionMouse
{
	RAWINPUTDEVICE Rid[1];
	Rid[0].usUsagePage = 0x01;
	Rid[0].usUsage = 0x02;
	Rid[0].dwFlags = RIDEV_INPUTSINK;
	Rid[0].hwndTarget = hWnd;
	m_HighPrecisionMouseRegistered = (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) != 0);
}

void SetupGamepad()
{
	HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");
	if (!XInputLibrary)
	{
		XInputLibrary = LoadLibraryA("xinput9_1_0.dll");
	}

	if (!XInputLibrary)
	{
		XInputLibrary = LoadLibraryA("xinput1_3.dll");
	}

	if (XInputLibrary)
	{
		InputGetState = (TInputGetState *)GetProcAddress(XInputLibrary, "XInputGetState");
		if (!InputGetState)
		{
			InputGetState = InputGetStateStub;
		}

		InputSetState = (TInputSetState *)GetProcAddress(XInputLibrary, "XInputSetState");
		if (!InputSetState)
		{
			InputSetState = InputSetStateStub;
		}
	}
}

void CInputManager::KeyboardData::SetKey(unsigned char key, bool state)
{
	raw[key] = state;

	if (key >= VK_NUMPAD0 || key <= VK_NUMPAD9)
	{
		numpad[key - VK_NUMPAD0] = state;
	}
	else if (key >= VK_F1 || key <= VK_F24)
	{
		fkey[key - VK_F1] = state;
	}
	else
	{
		switch (key)
		{
		case VK_ESCAPE:
			escape = state;
			break;
		case VK_SPACE:
			space = state;
			break;
		case VK_LEFT:
			left = state;
			break;
		case VK_UP:
			up = state;
			break;
		case VK_RIGHT:
			right = state;
			break;
		case VK_DOWN:
			down = state;
			break;
		}
	}
}

bool CInputManager::HandleKeyboard(const MSG& msg)
{
	bool handled = false;

	unsigned char VKCode = (unsigned char)msg.wParam;

	int repeat = msg.lParam & 0xffff;
	bool AltKeyWasDown = ((msg.lParam & (1 << 29)) != 0);
	bool WasDown = ((msg.lParam & (1 << 30)) != 0);

	bool IsDown = ((msg.lParam & (1 << 31)) == 0);

	if (WasDown != IsDown)
	{
		m_KeyboardState.SetKey(VKCode, IsDown);
		handled = true;
	}
	return handled;
}

bool CInputManager::HandleMouse(const MSG& msg)
{
	switch (msg.message)
	{
	case WM_LBUTTONDOWN:
		m_ButtonLeft = true;
		return true;
	case WM_MBUTTONDOWN:
		m_ButtonMiddle = true;
		return true;
	case WM_RBUTTONDOWN:
		m_ButtonRight = true;
		return true;
	case WM_LBUTTONUP:
		m_ButtonLeft = false;
		return true;
	case WM_MBUTTONUP:
		m_ButtonMiddle = false;
		return true;
	case WM_RBUTTONUP:
		m_ButtonRight = false;
		return true;
	case WM_MOUSEMOVE:
		m_MouseX = LOWORD(msg.lParam);
		m_MouseY = HIWORD(msg.lParam);
		return true;
	case WM_INPUT:
	{
		UINT dwSize = 60;
		static BYTE lpb[60];

		UINT read = GetRawInputData((HRAWINPUT)msg.lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER));

		RAWINPUT* raw = (RAWINPUT*)lpb;

		if (raw->header.dwType == RIM_TYPEMOUSE && read <= dwSize)
		{
			m_MouseMovementX += raw->data.mouse.lLastX;
			m_MouseMovementY += raw->data.mouse.lLastY;

			return true;
		}
		return false;
	}
	default:
		return false;
	}
}

void CInputManager::PreUpdate(bool isWindowActive)
{
	m_PreviousKeyboardState = m_KeyboardState;

	m_PreviousButtonLeft = m_ButtonLeft;
	m_PreviousButtonMiddle = m_ButtonMiddle;
	m_PreviousButtonRight = m_ButtonRight;

	m_MouseMovementX = m_MouseMovementY = m_MouseMovementZ = 0;

	m_PreviousGamepadA = m_GamepadA;

	if (!isWindowActive)
	{
		m_KeyboardState = {};

		m_ButtonLeft = m_ButtonMiddle = m_ButtonRight = false;
	}
}

void CInputManager::PostUpdate()
{
	XINPUT_STATE l_ControllerState;
	if (InputGetState(0, &l_ControllerState) == ERROR_SUCCESS)
	{
		m_HasGamepad = true;
		m_GamepadStickLeftX = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		m_GamepadStickLeftY = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);

		m_GamepadA = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0;
	}
	else
	{
		m_HasGamepad = false;
	}
}


// ACTION MANAGER HEADER

struct ActionTrigger
{
	enum ButtonActionType
	{
		IsPressed, IsReleased, BecomesPressed, BecomesReleased
	};
	enum TriggerType {
		KEYBOARD,
		MOUSE,
		MOUSE_BUTTON,
		GAMEPAD,
		GAMEPAD_BUTTON
	} type;
	union
	{
		struct
		{
			ButtonActionType actionType;
			unsigned char keyCode;
			float value;
		} keyboard;
		struct MouseButton
		{
			ButtonActionType actionType;
			InputDefinitions::MouseButton button;
			float value;
		} mouseButton;
		struct
		{
			ButtonActionType actionType;
			InputDefinitions::GamepadButton button;
			float value;
		} gamepadButton;

		struct
		{
			float threshold;
			InputDefinitions::MouseAxis axis;
			bool geThreshold;
		} mouse;
		struct
		{
			float threshold;
			InputDefinitions::GamepadAxis axis;
			bool geThreshold;
		} gamepad;
	};


	static TriggerType GetTriggerTypeFromString(const char* str, TriggerType defaultValue = (TriggerType)-1);

	static ButtonActionType GetButtonActionTypeFromString(const char* str, ButtonActionType defaultValue = (ButtonActionType)-1);
};

struct InputAction
{
	float value;
	bool active;

	std::vector<ActionTrigger> triggers;
};

class CActionManager : public base::utils::CTemplatedMap<InputAction>
{
public:

	CActionManager(const CInputManager &inputManager);
	virtual ~CActionManager();

	bool LoadActions(const std::string &path);
	void SetDemoActions();

	void Update();

private:
	const CInputManager &m_InputManager;
};

// ACTION MANAGER CPP

TriggerType ActionTrigger::GetTriggerTypeFromString(const char* str, TriggerType defaultValue = (TriggerType)-1)
{
	if (str == nullptr)
		return defaultValue;
	else if (strcmp(str, "KEYBOARD") == 0)
		return KEYBOARD;
	else if (strcmp(str, "MOUSE") == 0)
		return MOUSE;
	else if (strcmp(str, "MOUSE_BUTTON") == 0)
		return MOUSE_BUTTON;
	else if (strcmp(str, "GAMEPAD") == 0)
		return GAMEPAD;
	else if (strcmp(str, "GAMEPAD_BUTTON") == 0)
		return GAMEPAD_BUTTON;
	else
		return (TriggerType)-1;
}

void CActionManager::Update()
{
	for (auto &actionIt : m_ResourcesMap)
	{
		InputAction *action = actionIt.second;
		action->active = false;
		action->value = 0;

		for (ActionTrigger& trigger : action->triggers)
		{
			switch (trigger.type)
			{
			case ActionTrigger::KEYBOARD:
				switch (trigger.keyboard.actionType)
				{
				case ActionTrigger::IsPressed:
					if (m_InputManager.IsKeyPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::IsReleased:
					if (!m_InputManager.IsKeyPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::BecomesPressed:
					if (m_InputManager.KeyBecomesPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::BecomesReleased:
					if (m_InputManager.KeyBecomesReleased(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				default:
					assert(false);
					break;
				}
				break;
			case ActionTrigger::MOUSE:
			{
				float value = m_InputManager.GetMouseAxis(trigger.mouse.axis);
				bool active = trigger.mouse.geThreshold ? (value >= trigger.mouse.threshold) : (value <= trigger.mouse.threshold);
				if (fabs(value) >= fabs(action->value))
				{
					if (active || !action->active)
					{
						action->value = value;
						action->active = active;
					}
				}
				break;
			}
			case ActionTrigger::MOUSE_BUTTON:
				switch (trigger.mouseButton.actionType)
				{
				case ActionTrigger::IsPressed:
					if (m_InputManager.IsMouseButtonPressed(trigger.mouseButton.button))
					{
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				case ActionTrigger::IsReleased:
				case ActionTrigger::BecomesPressed:
				case ActionTrigger::BecomesReleased:
					// TODO
					break;
				default:
					assert(false);
					break;
				}
				break;
			case ActionTrigger::GAMEPAD:
				// TODO
				break;
			case ActionTrigger::GAMEPAD_BUTTON:
				// TODO
				break;
			default:
				assert(false);
				break;
			}
		}
	}
}

void CActionManager::SetDemoActions()
{
	{
		InputAction action = {};

		ActionTrigger trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'A';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;

		action.triggers.push_back(trigger);

		base::utils::CTemplatedMap<InputAction>::Update("RotateLeft", new InputAction(action));
	}
	{
		InputAction action = {};

		ActionTrigger trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'D';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;

		action.triggers.push_back(trigger);

		base::utils::CTemplatedMap<InputAction>::Update("RotateRight", new InputAction(action));
	}
	{
		InputAction action = {};

		ActionTrigger trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'W';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;
		trigger.keyboard.value = 1;

		action.triggers.push_back(trigger);

		trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'S';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;
		trigger.keyboard.value = -1;

		action.triggers.push_back(trigger);

		trigger = {};
		trigger.type = ActionTrigger::MOUSE_BUTTON;
		trigger.mouseButton.button = InputDefinitions::LEFT;
		trigger.mouseButton.actionType = ActionTrigger::IsPressed;
		trigger.mouseButton.value = 1;

		action.triggers.push_back(trigger);

		trigger = {};
		trigger.type = ActionTrigger::MOUSE_BUTTON;
		trigger.mouseButton.button = InputDefinitions::RIGHT;
		trigger.mouseButton.actionType = ActionTrigger::IsPressed;
		trigger.mouseButton.value = -1;

		action.triggers.push_back(trigger);

		base::utils::CTemplatedMap<InputAction>::Update("Move", new InputAction(action));
	}
}

inline unsigned char GetKeyCode(const std::string& str)
{
	if (str.length() == 1 && ((str[0] >= 'A' && str[0] <= 'Z') || (str[0] >= '0' && str[0] <= '9')))
	{
		return str[0];
	}
	else if(str == "SPACE" || str == " ")
	{
		return VK_SPACE;
	}
	else
	{
		assert(false);
		return -1;
	}
}

bool CActionManager::LoadActions(const std::string &path)
{
	bool ok = false;

	tinyxml2::XMLDocument document;
	tinyxml2::XMLError error = document.LoadFile(path.c_str());
	if (base::xml::SucceedLoad(error))
	{
		tinyxml2::XMLElement *actions = document.FirstChildElement("actions");
		if (actions)
		{
			for (tinyxml2::XMLElement *action = actions->FirstChildElement(); action != nullptr; action = action->NextSiblingElement())
			{
				if (strcmp(action->Name(), "action") == 0)
				{
					InputAction inputAction = {};
					const char *actionName = action->Attribute("name");
					assert(actionName != nullptr);

					for (tinyxml2::XMLElement *trigger = action->FirstChildElement(); trigger != nullptr; trigger = trigger->NextSiblingElement())
					{
						if (strcmp(trigger->Name(), "trigger") == 0)
						{
							ActionTrigger actionTrigger = {};

							actionTrigger.type = ActionTrigger::GetTriggerTypeFromString(trigger->Attribute("type"));
							switch (actionTrigger.type)
							{
							case ActionTrigger::KEYBOARD:
								actionTrigger.keyboard.keyCode = GetKeyCode(trigger->Attribute("key_code"));
								actionTrigger.keyboard.actionType = ActionTrigger::GetButtonActionTypeFromString(trigger->Attribute("button_type"), ActionTrigger::IsPressed);
								actionTrigger.keyboard.value = trigger->FloatAttribute("value");
								break;
							case ActionTrigger::MOUSE:
								assert(false);
								break;
							case ActionTrigger::MOUSE_BUTTON:
								actionTrigger.mouseButton.button = InputDefinitions::GetMouseButtonFromString(trigger->Attribute("button"), InputDefinitions::LEFT);
								actionTrigger.mouseButton.actionType = ActionTrigger::GetButtonActionTypeFromString(trigger->Attribute("button_type"), ActionTrigger::IsPressed);
								actionTrigger.mouseButton.value = trigger->FloatAttribute("value");
								break;
							case ActionTrigger::GAMEPAD:
							case ActionTrigger::GAMEPAD_BUTTON:
							default:
								assert(false);
								break;
							}

							inputAction.triggers.push_back(actionTrigger);
						}
						else
						{
							assert(false); // TODO better log error
						}
					}


					base::utils::CTemplatedMap<InputAction>::Update(actionName, new InputAction(inputAction));
				}
				else
				{
					assert(false); // TODO better log error
				}
			}
		}
		ok = true;
	}
	return ok;
}

// ON MAIN

while(true)
{
	l_InputManager.PreUpdate(s_WindowActive);

    while( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
    {
		bool fHandled = false;
		if ((msg.message >= WM_MOUSEFIRST && msg.message <= WM_MOUSELAST) || msg.message == WM_INPUT)
		{
			fHandled = l_InputManager.HandleMouse(msg);
		}
		else if (msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST)
		{
			fHandled = l_InputManager.HandleKeyboard(msg);
		}

		if (!fHandled)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
			break;
    }

	{
		l_InputManager.PostUpdate();
		
	
		// update
		
		// render
	}
}


// on msgproc:
  case WM_ACTIVATE:
  {
	  s_WindowActive = wParam != WA_INACTIVE;
	  break;
  }
