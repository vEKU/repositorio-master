#include "Engine\Camera\FPSCameraController.h"


void CFPSCameraController::Update(float ElapsedTime) {
	m_yaw -= m_yawSpeed * m_sensitivity * ElapsedTime;
	m_pitch += m_pitchSpeed * m_sensitivity * ElapsedTime;

	if (m_yaw > m_maxYaw) {
		m_yaw -= mathUtils::PiTimes<float>(2.0f);
	}
	if (m_yaw < m_minYaw) {
		m_yaw += mathUtils::PiTimes<float>(2.0f);
	}
	if (m_pitch > m_maxPitch) {
		m_pitch = m_maxPitch;
	}
	if (m_pitch < m_minPitch) {
		m_pitch = m_minPitch;
	}

	m_Front.x = sin(m_yaw) * cos(m_pitch);
	m_Front.y = -sin(m_pitch);
	m_Front.z = cos(m_yaw) * cos(m_pitch);

	m_Position.x += (sin(m_yaw) * cos(m_pitch) * m_zSpeed) + (-cos(m_yaw) * m_xSpeed);
	m_Position.y += sin(-m_pitch) * m_zSpeed;
	m_Position.z += (cos(m_yaw) * cos(m_pitch) * m_zSpeed) + (sin(m_yaw) * m_xSpeed);


	m_yawSpeed = 0.0f;
	m_pitchSpeed = 0.0f;
	m_zSpeed = 0.0f;
	m_xSpeed = 0.0f;

}

void CFPSCameraController::DisplayImguiInfo() {
	ImGui::Begin("Camera Info", false, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::SliderFloat("Pitch", &m_pitch, m_minPitch, m_maxPitch);
	ImGui::SliderFloat("Yaw", &m_yaw, -mathUtils::PiTimes<float>(), mathUtils::PiTimes<float>());

	if (ImGui::CollapsingHeader("Speed")) {
		ImGui::Text("Pitch Speed: %f", m_pitchSpeed);
		ImGui::Text("Yaw Speed: %f", m_yawSpeed);
		ImGui::Text("zSpeed: %f", m_zSpeed);
		ImGui::Text("xSpeed: %f", m_xSpeed);
	}


	if (ImGui::CollapsingHeader("Front")) {
		ImGui::Text("m_Front.x: %f", m_Front.x);
		ImGui::Text("m_Front.y: %f", m_Front.y);
		ImGui::Text("m_Front.z: %f", m_Front.z);
	}

	if (ImGui::CollapsingHeader("Position")) {
		ImGui::Text("m_Position.x: %f", m_Position.x);
		ImGui::Text("m_Position.y: %f", m_Position.y);
		ImGui::Text("m_Position.z: %f", m_Position.z);
	}

	if (ImGui::CollapsingHeader("Up")) {
		ImGui::Text("m_Up.x: %f", m_Up.x);
		ImGui::Text("m_Up.y: %f", m_Up.y);
		ImGui::Text("m_Up.z: %f", m_Up.z);
	}

	ImGui::End();
}

void CFPSCameraController::HandleActions(CActionManager* am) {

	if (am->Get("MoveZ")->active) {
		m_zSpeed = am->Get("MoveZ")->value;
	} else {
		m_zSpeed = 0;
	}
	if (am->Get("MoveX")->active) {
		m_xSpeed = am->Get("MoveX")->value;
	} else {
		m_xSpeed = 0;
	}

	if (am->Get("Pitch")->active) {
		m_pitchSpeed = am->Get("Pitch")->value;
	}
	if (am->Get("Yaw")->active) {
		m_yawSpeed = am->Get("Yaw")->value;
	}

	if (am->Get("PitchGamepad")->active) {
		m_pitchSpeed = am->Get("PitchGamepad")->value * (-m_gamepadSensitivity); //Invertido (Coordenadas motor) y aumentado
	}
	if (am->Get("YawGamepad")->active) {		
		m_yawSpeed = am->Get("YawGamepad")->value * (m_gamepadSensitivity);
	}
}