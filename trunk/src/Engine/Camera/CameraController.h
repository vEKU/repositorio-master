#ifndef  CAMERA_CONTROLLER
#define  CAMERA_CONTROLLER

#include "Base\Math\Vector3.h"
#include "Engine\Render\RenderManager.h"
#include "Base\DearImGUI\imgui.h"
#include "Engine\Input\ActionManager.h"

class CCameraController
{
public:
	CCameraController() :m_Position(0, 1, 0), m_Front(0, 0, 1), m_Up(0, 1, 0){}
	virtual ~CCameraController() {}
	virtual void Update(float ElapsedTime) = 0;
	virtual void DisplayImguiInfo(){};
	virtual void HandleActions(CActionManager* am){};

	void SetToRenderManager(CRenderManager &_RenderManager) const
	{
		_RenderManager.SetViewMatrix(m_Position, m_Position + m_Front, m_Up);
	}
protected:
	Vect3f m_Position;
	Vect3f m_Front;
	Vect3f m_Up;
};

#endif // ! CAMERA_CONTROLLER
