#ifndef SPHERICAL_CAMERA_CONTROLLER
#define SPHERICAL_CAMERA_CONTROLLER

#include "Engine\Camera\CameraController.h"
#include "Base\Math\Vector3.h"

class CSphericalCameraController : public CCameraController
{
public:
	CSphericalCameraController() : m_center(0, 0 ,0), m_yawSpeed(0), 
		m_pitchSpeed(0), m_rollSpeed(0), m_zoomSpeed(0), m_yaw(0), m_pitch(0),
		m_roll(), m_zoom(10.0f), m_maxPitch(mathUtils::PiTimes<float>(0.5f)),
		m_minPitch(mathUtils::PiTimes<float>(-0.5f)),m_maxZoom(50.0f), m_minZoom(3.0f),
		m_zoomSensitivity(0.2f){};
	Vect3f m_center;
	float m_yawSpeed, m_pitchSpeed, m_rollSpeed;
	float m_zoomSpeed;

private:
	float m_yaw, m_pitch, m_roll, m_zoom;
	float m_maxPitch, m_minPitch;
	float m_maxZoom, m_minZoom;
	float m_zoomSensitivity;
public:

	virtual void Update(float ElapsedTime);
	virtual void DisplayImguiInfo() override;
	virtual void HandleActions(CActionManager* am) override;
};
#endif // !SPHERICAL_CAMERA_CONTROLLER
