#ifndef FPS_CAMERA_CONTROLLER
#define FPS_CAMERA_CONTROLLER

#include "Engine\Camera\CameraController.h"

class CFPSCameraController : public CCameraController {
public:
	CFPSCameraController() : m_yawSpeed(0), m_pitchSpeed(0), m_zSpeed(0), m_xSpeed(0), m_sensitivity(0.2f), 
		m_gamepadSensitivity(10.0f), m_yaw(0), m_pitch(0),m_maxPitch(mathUtils::PiTimes<float>(0.49f)),
		m_minPitch(mathUtils::PiTimes<float>(-0.49f)),m_maxYaw(mathUtils::PiTimes<float>(1.0f)), 
		m_minYaw(mathUtils::PiTimes<float>(-1.0f)){};

	float m_yawSpeed, m_pitchSpeed;
	float m_zSpeed, m_xSpeed;
	float m_sensitivity, m_gamepadSensitivity;
private:
	float m_yaw, m_pitch;
	float m_maxPitch, m_minPitch, m_maxYaw, m_minYaw;
public:
	virtual void Update(float ElapsedTime);
	virtual void DisplayImguiInfo() override;
	virtual void HandleActions(CActionManager* am) override;


};
#endif // !FPS_CAMERA_CONTROLLER