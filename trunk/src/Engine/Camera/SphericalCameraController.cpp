#include "Engine\Camera\SphericalCameraController.h"

void CSphericalCameraController::Update(float ElapsedTime) {

	m_yaw += m_yawSpeed * ElapsedTime;
	m_pitch += m_pitchSpeed * ElapsedTime;
	m_roll += m_rollSpeed * ElapsedTime;
	m_zoom -= m_zoomSpeed * m_zoomSensitivity * ElapsedTime;

	if (m_zoom > m_maxZoom) {
		m_zoom = m_maxZoom;
	}
	if (m_zoom < m_minZoom) {
		m_zoom = m_minZoom;
	}

	if (m_pitch > m_maxPitch) {
		m_pitch = m_maxPitch;
	}
	if (m_pitch < m_minPitch) {
		m_pitch = m_minPitch;
	}

	if (m_yaw > mathUtils::PiTimes<float>()) {
		m_yaw -= mathUtils::PiTimes<float>(2.0f);
	}
	if (m_yaw < -mathUtils::PiTimes<float>()) {
		m_yaw += mathUtils::PiTimes<float>(2.0f);
	}

	if (m_roll > mathUtils::PiTimes<float>()) {
		m_roll -= mathUtils::PiTimes<float>(2.0f);
	}
	if (m_roll < -mathUtils::PiTimes<float>()) {
		m_roll += mathUtils::PiTimes<float>(2.0f);
	}

	m_Front.x = sin(m_yaw) * cos(-m_pitch);
	m_Front.y = -sin(-m_pitch);
	m_Front.z = -cos(m_yaw) * cos(-m_pitch);

	m_Up.x = sin(m_roll);
	m_Up.y = cos(m_roll);


	m_Position = m_center - m_Front * m_zoom;

}

void CSphericalCameraController::DisplayImguiInfo() {
	ImGui::Begin("Camera Info", false, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::SliderFloat("Pitch", &m_pitch, m_minPitch, m_maxPitch);
	ImGui::SliderFloat("Yaw", &m_yaw, -mathUtils::PiTimes<float>(), mathUtils::PiTimes<float>());
	ImGui::SliderFloat("Roll", &m_roll, -mathUtils::PiTimes<float>(0.5f), mathUtils::PiTimes<float>(0.5f));
	ImGui::SliderFloat("Zoom", &m_zoom, m_minZoom, m_maxZoom);

	ImGui::SliderFloat("Pitch Speed", &m_pitchSpeed, -1.0f, 1.0f);
	ImGui::SliderFloat("Yaw Speed", &m_yawSpeed, -1.0f, 1.0f);
	ImGui::SliderFloat("Roll Speed", &m_rollSpeed, -1.0f, 1.0f);
	ImGui::SliderFloat("Zoom Speed", &m_zoomSpeed, -1.0f, 1.0f);

	ImGui::Text("m_Front.x: %f", m_Front.x);
	ImGui::Text("m_Front.y: %f", m_Front.y);
	ImGui::Text("m_Front.z: %f", m_Front.z);

	ImGui::Text("m_Up.x: %f", m_Up.x);
	ImGui::Text("m_Up.y: %f", m_Up.y);
	ImGui::Text("m_Up.z: %f", m_Up.z);

	ImGui::End();
}

void CSphericalCameraController::HandleActions(CActionManager* am) {

	if (am->Get("SphereYaw")->active) {
		m_yawSpeed = am->Get("SphereYaw")->value;
	} else {
		m_yawSpeed = 0;
	}

	if (am->Get("SpherePitch")->active) {
		m_pitchSpeed = am->Get("SpherePitch")->value;
	}
	else {
		m_pitchSpeed = 0;
	}

	if (am->Get("SphereRoll")->active) {
		m_rollSpeed = am->Get("SphereRoll")->value;
	}
	else {
		m_rollSpeed = 0;
	}

	if (am->Get("Zoom")->active) {
		m_zoomSpeed = am->Get("Zoom")->value;
	}
	else {
		m_zoomSpeed = 0;
	}
}