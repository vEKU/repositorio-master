#pragma once
#include <Windows.h>
#include <d3d11.h>
#include <d3dcompiler.h> //Substituye d3dx11.h
#include "Base\Math\Color.h"
#include "Base\Math\Matrix44.h"



class CRenderManager
{
public:
	struct CDebugVertex
	{
		Vect4f Position;
		CColor Color;
	};

	static const int DebugVertexBufferSize = 0x10000;
	int Width;
	int Height;
	HWND hWnd;


protected:
	ID3D11Device							*m_Device;
	ID3D11DeviceContext						*m_DeviceContext;
	IDXGISwapChain							*m_SwapChain;
	ID3D11RenderTargetView					*m_RenderTargetView;
	ID3D11Texture2D							*m_DepthStencil;
	ID3D11DepthStencilView					*m_DepthStencilView;

	
	ID3D11Buffer							*m_DebugVertexBuffer;
	int										m_DebugVertexBufferCurrentIndex;
	ID3D11VertexShader						*m_DebugVertexShader;
	ID3D11InputLayout						*m_DebugVertexLayout;
	ID3D11PixelShader						*m_DebugPixelShader;
	
	const CDebugVertex*						m_AxisRenderableVertexs;
	const CDebugVertex*						m_GridRenderableVertexs;
	const CDebugVertex*						m_CubeRenderableVertexs;
	const CDebugVertex*						m_SphereRenderableVertexs;

	int m_NumVerticesAxis, m_NumVerticesGrid, m_NumVerticesCube, m_NumVerticesSphere;
	/*
	ID3D11BlendState						*m_DrawQuadBlendState;

	ID3D11RasterizerState					*m_WireframeRenderState;*/
	ID3D11RasterizerState					*m_SolidRenderState;
	
	
	Mat44f m_ModelMatrix, m_ViewMatrix, m_ProjectionMatrix;
	Mat44f m_ViewProjectionMatrix, m_ModelViewProjectionMatrix;

	void DebugRender(const Mat44f& modelViewProj, const CDebugVertex* modelVertices, int numVertices, CColor colorTint);
	
public:
	Vect4f									m_BackgroundColor;

public:
	CRenderManager();
	~CRenderManager();

	bool Init(HWND hWnd, int Width, int Height);
	void Destroy();
	bool Get_RendertargetView();
	bool Create_DepthStencil();
	void Set_Viewport();
	void ActivateRenderTarget();
	bool CreateDebugShader();
	bool CreateDebugVertexBuffer();

	void Draw_Triangle();


	template < typename T > static void Release(T*& aHolder);

	ID3D11Device * GetDevice() const { return m_Device; }
	ID3D11DeviceContext * GetDeviceContext() const { return m_DeviceContext; }

	void BeginRender();
	void EndRender();

	
	void DrawAxis(float SizeX, float SizeY, float SizeZ, float rotation, const CColor &Color = colWHITE);
	void DrawAxis(Vect3f Size, float rotation, const CColor &Color = colWHITE) { DrawAxis(Size.x, Size.y, Size.z, rotation, Color); }
	void DrawAxis(float Size = 1.0f, float rotation = 0.0f, const CColor &Color = colWHITE) { DrawAxis(Size, Size, Size, rotation, Color); }

	void DrawGrid(float SizeX, float SizeY, float SizeZ, float rotation, const CColor &Color = colWHITE);
	void DrawGrid(Vect3f Size, float rotation, const CColor &Color = colWHITE) { DrawGrid(Size.x, Size.y, Size.z, rotation, Color); }
	void DrawGrid(float Size = 1.0f, float rotation = 0.0f, const CColor &Color = colWHITE) { DrawGrid(Size, Size, Size, rotation, Color); }

	void DrawCube(float SizeX, float SizeY, float SizeZ, float rotation, const CColor &Color = colWHITE);
	void DrawCube(Vect3f Size, float rotation, const CColor &Color = colWHITE) { DrawCube(Size.x, Size.y, Size.z, rotation, Color); }
	void DrawCube(float Size = 1.0f, float rotation = 0.0f, const CColor &Color = colWHITE) { DrawCube(Size, Size, Size, rotation, Color); }

	void DrawSphere(float Radius, float rotation, const CColor &Color = colWHITE);

	void SetModelMatrix(const Mat44f &Model) { m_ModelMatrix = Model; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
	void SetViewMatrix(const Mat44f &View) { m_ViewMatrix = View; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
	void SetProjectionMatrix(const Mat44f &Projection) { m_ProjectionMatrix = Projection; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }

	void SetViewProjectionMatrix(const Mat44f &View, const Mat44f &Projection) { m_ViewMatrix = View; m_ProjectionMatrix = Projection; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }

	void SetViewMatrix(const Vect3f& vPos, const Vect3f& vTarget, const Vect3f& vUp) { Mat44f View; View.SetIdentity(); View.SetFromLookAt(vPos, vTarget, vUp); SetViewMatrix(View); }
	void SetProjectionMatrix(float fovy, float aspect, float zn, float zf) { Mat44f Projection; Projection.SetIdentity(); Projection.SetFromPerspective(fovy, aspect, zn, zf); SetProjectionMatrix(Projection); }
	
	void SetSolidRenderState();
	void SetWireframeRenderState();
};
