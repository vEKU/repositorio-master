#ifndef ENGINE
#define ENGINE

#include "Base\Utils\Singleton.h"

#include "Engine\Input\ActionManager.h"
#include "Engine\Render\RenderManager.h"
#include "Engine\Camera\CameraController.h"
#include "Engine\Camera\FPSCameraController.h"
#include "Engine\Camera\SphericalCameraController.h"
#include "Engine\Materials\MaterialManager.h"

#include "Base\DearImGUI\imgui.h"



#include <chrono>


//GETTERS AND SETTERS
#define BUILD_GET_SET_ENGINE_MANAGER(Manager)\
private: \
	C##Manager* m_##Manager; \
public: \
	void Set##Manager(C##Manager* a##Manager) { m_##Manager = a##Manager; }\
	const C##Manager& Get##Manager() const { return *m_##Manager; } \
	C##Manager& Get##Manager() { return *m_##Manager; }\
// END COMMENTS AND SETTERS


class CRenderManager;
class CActionManager;
class CCameraController;
class CMaterialManager;

class CEngine : public base::utils::CSingleton<CEngine> {

public:

	BUILD_GET_SET_ENGINE_MANAGER(RenderManager);
	BUILD_GET_SET_ENGINE_MANAGER(ActionManager);
	BUILD_GET_SET_ENGINE_MANAGER(CameraController);
	BUILD_GET_SET_ENGINE_MANAGER(MaterialManager);

	CEngine();
	virtual ~CEngine();

	void ProcessInputs();
	void Update();
	void Render();

private:
	std::chrono::monotonic_clock m_Clock;
	std::chrono::monotonic_clock::time_point m_PrevTime;
	signed int cameraType;
};

#endif