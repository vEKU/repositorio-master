#include "Engine\Engine.h"

CEngine::CEngine() {
	m_PrevTime = m_Clock.now();
	cameraType = 0;
}

CEngine::~CEngine() {

}


void CEngine::ProcessInputs() {
	m_ActionManager->Update();
	m_CameraController->HandleActions(m_ActionManager);
}

void CEngine::Update() {

	auto currentTime = m_Clock.now();
	std::chrono::duration<float> chronoDeltaTime = currentTime - m_PrevTime;
	m_PrevTime = currentTime;
	float deltaTime = chronoDeltaTime.count() > 0.5f ? 0.5f : chronoDeltaTime.count();

	if (m_ActionManager->Get("ChangeCamera")->active ) {
		CCameraController *newCamera;
		if (cameraType == 0) {
			newCamera = new CFPSCameraController();
			cameraType = 1;
		}
		else {
			newCamera = new CSphericalCameraController();
			cameraType = 0;
		}
		delete &GetCameraController(); //Destruyo la camara actual
		SetCameraController(newCamera); //A�ado la nueva camara
	}

	//IMGUI//
	ImGui::Begin("Engine Info", false, ImGuiWindowFlags_AlwaysAutoResize);
	if (ImGui::Button("Spherical Camera")) {
		cameraType = 0;
		CCameraController *newCamera = new CSphericalCameraController();
		delete &GetCameraController();
		SetCameraController(newCamera);
	}
	if (ImGui::Button("FPS Camera")) {
		cameraType = 1;
		CCameraController *newCamera = new CFPSCameraController();
		delete &GetCameraController();
		SetCameraController(newCamera);
	}
	ImGui::End();


	//END IMGUI//


	m_CameraController->DisplayImguiInfo();
	m_CameraController->Update(deltaTime);
	m_CameraController->SetToRenderManager(*m_RenderManager);
}

void CEngine::Render() {
	m_RenderManager->BeginRender();
	m_RenderManager->DrawSphere(4.0f, 0.0f);
	m_RenderManager->DrawGrid(10.0f, 0.0f);
	ImGui::Render();
	m_RenderManager->EndRender();
}