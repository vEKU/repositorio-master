
// Render Manager Class
class CRenderManager
{
public:
	struct CDebugVertex
	{
		Vect4f Position;
		CColor Color;
	};

	static const int DebugVertexBufferSize = 0x10000;

protected:
	ID3D11Device							*m_Device;
	ID3D11DeviceContext						*m_DeviceContext;
	IDXGISwapChain							*m_SwapChain;
	ID3D11RenderTargetView					*m_RenderTargetView;
	ID3D11Texture2D							*m_DepthStencil;
	ID3D11DepthStencilView					*m_DepthStencilView;

	ID3D11Buffer							*m_DebugVertexBuffer;
	int										m_DebugVertexBufferCurrentIndex;
	ID3D11VertexShader						*m_DebugVertexShader;
	ID3D11InputLayout						*m_DebugVertexLayout;
	ID3D11PixelShader						*m_DebugPixelShader;

	const CDebugVertex*						m_AxisRenderableVertexs;
	const CDebugVertex*						m_GridRenderableVertexs;
	const CDebugVertex*						m_CubeRenderableVertexs;
	const CDebugVertex*						m_SphereRenderableVertexs;

	int m_NumVerticesAxis, m_NumVerticesGrid, m_NumVerticesCube, m_NumVerticesSphere;

	ID3D11BlendState						*m_DrawQuadBlendState;

	ID3D11RasterizerState					*m_WireframeRenderState;
	ID3D11RasterizerState					*m_SolidRenderState;

	Mat44f m_ModelMatrix, m_ViewMatrix, m_ProjectionMatrix;
	Mat44f m_ViewProjectionMatrix, m_ModelViewProjectionMatrix;

	void DebugRender(const Mat44f& modelViewProj, const CDebugVertex* modelVertices, int numVertices, CColor colorTint);

public:
	Vect4f									m_BackgroundColor;

public:
	CRenderManager();
	virtual ~CRenderManager();
	bool Init(HWND hWnd, int Width, int Height);
	void Destroy();

  template < typename T > static void Release(T*& aHolder );
	
	ID3D11Device * GetDevice() const {return m_Device;}
	ID3D11DeviceContext * GetDeviceContext() const {return m_DeviceContext;}
	
	void BeginRender();
	void EndRender();

	void DrawAxis(float SizeX, float SizeY, float SizeZ, const CColor &Color = colWHITE);
	void DrawGrid(float SizeX, float SizeY, float SizeZ, const CColor &Color = colWHITE);
	void DrawCube(float SizeX, float SizeY, float SizeZ, const CColor &Color = colWHITE);
	void DrawSphere(float Radius, const CColor &Color = colWHITE);

	void SetModelMatrix(const Mat44f &Model) { m_ModelMatrix = Model; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
	void SetViewMatrix(const Mat44f &View) { m_ViewMatrix = View; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
	void SetProjectionMatrix(const Mat44f &Projection) { m_ProjectionMatrix = Projection; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
	
	void SetViewProjectionMatrix(const Mat44f &View, const Mat44f &Projection) { m_ViewMatrix = View; m_ProjectionMatrix = Projection; m_ViewProjectionMatrix = m_ViewMatrix * m_ProjectionMatrix; m_ModelViewProjectionMatrix = m_ModelMatrix * m_ViewProjectionMatrix; }
	
	void SetViewMatrix(const Vect3f& vPos, const Vect3f& vTarget, const Vect3f& vUp) { Mat44f View; View.SetIdentity(); View.SetFromLookAt(vPos, vTarget, vUp); SetViewMatrix(View); }
	void SetProjectionMatrix(float fovy, float aspect, float zn, float zf) { Mat44f Projection; Projection.SetIdentity(); Projection.SetFromPerspective(fovy, aspect, zn, zf); SetProjectionMatrix(Projection); }


	void DrawAxis(Vect3f Size, const CColor &Color = colWHITE) { DrawAxis(Size.x, Size.y, Size.z, Color); }
	void DrawAxis(float Size = 1.0f, const CColor &Color = colWHITE) { DrawAxis(Size, Size, Size, Color); }
	void DrawGrid(Vect3f Size, const CColor &Color = colWHITE) { DrawGrid(Size.x, Size.y, Size.z, Color); }
	void DrawGrid(float Size = 1.0f, const CColor &Color = colWHITE) { DrawGrid(Size, Size, Size, Color); }
	void DrawCube(Vect3f Size, const CColor &Color = colWHITE) { DrawCube(Size.x, Size.y, Size.z, Color); }
	void DrawCube(float Size = 1.0f, const CColor &Color = colWHITE) { DrawCube(Size, Size, Size, Color); }

	void SetSolidRenderState();
	void SetWireframeRenderState();
};

// main

LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
  switch( msg )
  {
  case WM_DESTROY:
    {
      PostQuitMessage( 0 );
      return 0;
    }
	break;
  case WM_ACTIVATE:
  {
	  s_WindowActive = wParam != WA_INACTIVE;
	  break;
  }
  case WM_KEYDOWN:
    {
      switch( wParam )
      {
      case VK_ESCAPE:
        //Cleanup();
        PostQuitMessage( 0 );
        return 0;
        break;
      }
    }
    break;
  }//end switch( msg )

  return DefWindowProc( hWnd, msg, wParam, lParam );
}

int APIENTRY WinMain(HINSTANCE _hInstance, HINSTANCE _hPrevInstance, LPSTR _lpCmdLine, int _nCmdShow)
{
  // Register the window class
  WNDCLASSEX wc = {	sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, APPLICATION_NAME, NULL };
  
  RegisterClassEx( &wc ); 

  RECT rc = { 0, 0, 800, 600 };
  AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

  // Create the application's window
  HWND hWnd = CreateWindow(APPLICATION_NAME, APPLICATION_NAME, WS_OVERLAPPEDWINDOW, 100, 100, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, wc.hInstance, NULL);


  // A�adir aqu� el Init de la applicacio�n

  CRenderManager l_RenderManager;

  l_RenderManager.Init(hWnd, 800, 600);
  
  ShowWindow( hWnd, SW_SHOWDEFAULT );
  UpdateWindow( hWnd );
  MSG msg;
  ZeroMemory( &msg, sizeof(msg) );

  l_RenderManager.SetProjectionMatrix(0.8f, 800.0f / 600.0f, 0.5f, 100.5f);

  //std::chrono::high_resolution_clock clock;

  while( msg.message != WM_QUIT )
  {
    while( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
    {
		bool fHandled = false;

		if (!fHandled)
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}

		if (msg.message == WM_QUIT)
			break;
    }

	{
		l_RenderManager.BeginRender();
		
		// Render
		
		l_RenderManager.EndRender();
    }
  }
  UnregisterClass( APPLICATION_NAME, wc.hInstance );
  return 0;
}


// Initialization
void InitDevice_SwapChain_DeviceContext()
{
	D3D_FEATURE_LEVEL featureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };
	UINT numFeatureLevels = ARRAYSIZE( featureLevels );

	DXGI_SWAP_CHAIN_DESC sd;
    ZeroMemory( &sd, sizeof(sd) );
    sd.BufferCount = 1;
    sd.BufferDesc.Width = Width;
    sd.BufferDesc.Height = Height;
    sd.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
    sd.BufferDesc.RefreshRate.Numerator = 60;
    sd.BufferDesc.RefreshRate.Denominator = 1;
    sd.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
    sd.OutputWindow = hWnd;
    sd.SampleDesc.Count = 1;
    sd.SampleDesc.Quality = 0;
    sd.Windowed = TRUE;

    if(FAILED(D3D11CreateDeviceAndSwapChain( NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevels, numFeatureLevels,
                     D3D11_SDK_VERSION, &sd, &m_SwapChain, &m_Device, NULL, &m_DeviceContext ) ) )
    {
        return FALSE;
    }
}

void Get_RendertargetView()
{
	ID3D11Texture2D *pBackBuffer;
    if( FAILED( m_SwapChain->GetBuffer( 0, __uuidof( ID3D11Texture2D ), (LPVOID*)&pBackBuffer ) ) )
        return FALSE;
    HRESULT hr=m_Device->CreateRenderTargetView( pBackBuffer, NULL, &m_RenderTargetView );
    pBackBuffer->Release();
    if( FAILED( hr ) )
        return FALSE;
}

void Create_DepthStencil()
{
	D3D11_TEXTURE2D_DESC descDepth;
	ZeroMemory( &descDepth, sizeof(descDepth) );
    descDepth.Width = Width;
    descDepth.Height = Height;
    descDepth.MipLevels = 1;
    descDepth.ArraySize = 1;
    descDepth.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
    descDepth.SampleDesc.Count = 1;
    descDepth.SampleDesc.Quality = 0;
    descDepth.Usage = D3D11_USAGE_DEFAULT;
    descDepth.BindFlags = D3D11_BIND_DEPTH_STENCIL;
    descDepth.CPUAccessFlags = 0;
    descDepth.MiscFlags = 0;
    hr=m_Device->CreateTexture2D( &descDepth, NULL, &m_DepthStencil);
    if(FAILED(hr))
        return false;

    D3D11_DEPTH_STENCIL_VIEW_DESC descDSV;
	ZeroMemory( &descDSV, sizeof(descDSV) );
    descDSV.Format = descDepth.Format;
    descDSV.ViewDimension = D3D11_DSV_DIMENSION_TEXTURE2D;
    descDSV.Texture2D.MipSlice = 0;
    hr=m_Device->CreateDepthStencilView( m_DepthStencil, &descDSV, &m_DepthStencilView );
    if(FAILED(hr))
        return false;
}

void SetRendertarget()
{
	m_DeviceContext->OMSetRenderTargets( 1, &m_RenderTargetView, m_DepthStencilView );
}

void Set_Viewport()
{
	D3D11_VIEWPORT vp;
    vp.Width=(FLOAT)Width;
    vp.Height=(FLOAT)Height;
    vp.MinDepth=0.0f;
    vp.MaxDepth=1.0f;
    vp.TopLeftX=0;
    vp.TopLeftY=0;
    m_DeviceContext->RSSetViewports( 1, &vp );
}

void CreateDebugShader()
{
	// C++ macros are nuts
#define STRINGIFY(X) #X
#define TOSTRING(X) STRINGIFY(X)

	const char debugRenderEffectCode[] =
		"#line " TOSTRING(__LINE__) "\n"
		"struct VS_OUTPUT\n"
		"{\n"
		"	float4 Pos : SV_POSITION;\n"
		"	float4 Color : COLOR0;\n"
		"};\n"
		"\n"
		"VS_OUTPUT VS(float4 Pos : POSITION, float4 Color : COLOR)\n"
		"{\n"
		"	VS_OUTPUT l_Output = (VS_OUTPUT)0;\n"
		"	l_Output.Pos = Pos;\n"
		"	l_Output.Color = Color;\n"
		"	return l_Output;\n"
		"}\n"
		"\n"
		"float4 PS(VS_OUTPUT IN) : SV_Target\n"
		"{\n"
		"	//return float4(1,0,0,1);\n"
		"	//return m_BaseColor;\n"
		"	return IN.Color;\n"
		"}\n";

	ID3DBlob *vsBlob, *psBlob;
	ID3DBlob *errorBlob;

	D3DX11CompileFromMemory(debugRenderEffectCode, sizeof(debugRenderEffectCode), __FILE__, nullptr, nullptr,
		"VS", "vs_4_0", 0, 0, nullptr, &vsBlob, &errorBlob, &hr);

	if (FAILED(hr))
	{
		if (errorBlob != NULL)
			OutputDebugStringA((char*)errorBlob->GetBufferPointer());
		if (errorBlob)
			errorBlob->Release();
		return false;
	}
	D3DX11CompileFromMemory(debugRenderEffectCode, sizeof(debugRenderEffectCode), __FILE__, nullptr, nullptr,
		"PS", "ps_4_0", 0, 0, nullptr, &psBlob, &errorBlob, &hr);

	if (FAILED(hr))
	{
		if (errorBlob != NULL)
			OutputDebugStringA((char*)errorBlob->GetBufferPointer());
		if (errorBlob)
			errorBlob->Release();
		return false;
	}



	hr = m_Device->CreateVertexShader(vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), NULL, &m_DebugVertexShader);

	if(FAILED(hr))
		return false;

	hr = m_Device->CreatePixelShader(psBlob->GetBufferPointer(), psBlob->GetBufferSize(), NULL, &m_DebugPixelShader);

	if(FAILED(hr))
		return false;

	D3D11_INPUT_ELEMENT_DESC layout[] = { 
		{ "POSITION", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0 },
		{ "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, 16, D3D11_INPUT_PER_VERTEX_DATA, 0 }
	};
	hr = m_Device->CreateInputLayout(layout, 2, vsBlob->GetBufferPointer(), vsBlob->GetBufferSize(), &m_DebugVertexLayout);

	if(FAILED(hr))
		return false;

	D3D11_BUFFER_DESC l_BufferDescription;
	ZeroMemory(&l_BufferDescription, sizeof(l_BufferDescription));
	l_BufferDescription.Usage = D3D11_USAGE_DYNAMIC;
	l_BufferDescription.ByteWidth = sizeof(CDebugVertex)*DebugVertexBufferSize;
	l_BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
	l_BufferDescription.CPUAccessFlags = D3D11_CPU_ACCESS_WRITE;
	//D3D11_SUBRESOURCE_DATA InitData;
	//ZeroMemory(&InitData, sizeof(InitData));
	//InitData.pSysMem = Vtxs;
	hr = m_Device->CreateBuffer(&l_BufferDescription, nullptr, &m_DebugVertexBuffer);
	if (FAILED(hr))
		return false;
}

void CreateDebugObjects()
{
	//AXIS
	static CDebugVertex l_AxisVtxs[6] =
	{
		{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(1.0f, 0.0f, 0.0f, 1.0f) },
		{ Vect4f(1.0f, 0.0f, 0.0f, 1.0f), CColor(1.0f, 0.0f, 0.0f, 1.0f) },
			
		{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) },
		{ Vect4f(0.0f, 1.0f, 0.0f, 1.0f), CColor(0.0f, 1.0f, 0.0f, 1.0f) },

		{ Vect4f(0.0f, 0.0f, 0.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) },
		{ Vect4f(0.0f, 0.0f, 1.0f, 1.0f), CColor(0.0f, 0.0f, 1.0f, 1.0f) }
	};
	
	m_AxisRenderableVertexs = l_AxisVtxs;
	m_NumVerticesAxis = 6;
	
	
	//CUBE
	const float l_SizeCube=1.0f;
	static CDebugVertex l_CubeVtxs[] =
	{
		{ Vect4f(-l_SizeCube / 2.0f, -l_SizeCube / 2.0f, -l_SizeCube / 2.0f, 1.0f), CColor(1.0f, 1.0f, 1.0f, 1.0f) },
		// TODO
	};

	m_CubeRenderableVertexs = l_CubeVtxs;
	m_NumVerticesCube = 8 * 3;
	
	//GRID
	float l_Size=10.0f;
	const int l_Grid=10;
	static CDebugVertex l_GridVtxs[(l_Grid + 1) * 2 * 2];
	for(int b=0;b<=l_Grid;++b)
	{
		l_GridVtxs[b * 2].Position = Vect4f(); // TODO
		l_GridVtxs[b * 2].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
		l_GridVtxs[(b * 2) + 1].Position = Vect4f(); // TODO
		l_GridVtxs[(b * 2) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
	}
	//LINEAS EN X
	for(int b=0;b<=l_Grid;++b)
	{
		l_GridVtxs[(l_Grid + 1) * 2 + (b * 2)].Position = Vect4f(); // TODO
		l_GridVtxs[(l_Grid + 1) * 2 + (b * 2)].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
		l_GridVtxs[(l_Grid + 1) * 2 + (b * 2) + 1].Position = Vect4f(); // TODO
		l_GridVtxs[(l_Grid + 1) * 2 + (b * 2) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
	}

	m_GridRenderableVertexs = l_GridVtxs;
	m_NumVerticesGrid = (l_Grid + 1) * 2 * 2;
	
	//SPHERE
	const int l_Aristas=10;
	static CDebugVertex l_SphereVtxs[4 * l_Aristas*l_Aristas];
	for(int t=0;t<l_Aristas;++t)
	{
		float l_RadiusRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) * (180.0f*((float)t)) / ((float)l_Aristas));
		for(int b=0;b<l_Aristas;++b)
		{
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 0].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)(b + 1)) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)(b + 1)) / ((float)l_Aristas))), 1.0f);
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 1].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			
			float l_RadiusNextRing = sin(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)(t + 1))) / ((float)l_Aristas));
			
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Position = Vect4f(l_RadiusRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)t)) / ((float)l_Aristas)), l_RadiusRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 2].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Position = Vect4f(l_RadiusNextRing*cos(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), cos(/*DEG2RAD*/(6.28318531f / 360.f) *(180.0f*((float)(t + 1))) / ((float)l_Aristas)), l_RadiusNextRing*sin(/*DEG2RAD*/(6.28318531f / 360.f) *((float)(360.0f*(float)b) / ((float)l_Aristas))), 1.0f);
			l_SphereVtxs[(t*l_Aristas * 4) + (b * 4) + 3].Color = CColor(1.0f, 1.0f, 1.0f, 1.0f);
		}
	}
	
	m_SphereRenderableVertexs = l_SphereVtxs;
	m_NumVerticesSphere = 4 * l_Aristas*l_Aristas;
}

void Create_AlphaState()
{
	D3D11_BLEND_DESC l_AlphablendDesc;
	ZeroMemory(&l_AlphablendDesc, sizeof(D3D11_BLEND_DESC));
	l_AlphablendDesc.RenderTarget[0].BlendEnable=true;
	l_AlphablendDesc.RenderTarget[0].SrcBlend=D3D11_BLEND_SRC_ALPHA;
	l_AlphablendDesc.RenderTarget[0].DestBlend=D3D11_BLEND_INV_SRC_ALPHA;
	l_AlphablendDesc.RenderTarget[0].BlendOp=D3D11_BLEND_OP_ADD;
	l_AlphablendDesc.RenderTarget[0].SrcBlendAlpha=D3D11_BLEND_ONE;
	l_AlphablendDesc.RenderTarget[0].DestBlendAlpha=D3D11_BLEND_ONE;
	l_AlphablendDesc.RenderTarget[0].BlendOpAlpha=D3D11_BLEND_OP_ADD;
	l_AlphablendDesc.RenderTarget[0].RenderTargetWriteMask=D3D11_COLOR_WRITE_ENABLE_ALL;

	if(FAILED(m_Device->CreateBlendState(&l_AlphablendDesc, &m_DrawQuadBlendState)))
		return false;
}

void Create_RasterizerStates()
{
	//RENDER STATES
	D3D11_RASTERIZER_DESC l_WireframeDesc;
	ZeroMemory(&l_WireframeDesc, sizeof(D3D11_RASTERIZER_DESC));
	l_WireframeDesc.FillMode=D3D11_FILL_WIREFRAME;
	l_WireframeDesc.CullMode=D3D11_CULL_NONE;
	
	HRESULT l_HR=m_Device->CreateRasterizerState(&l_WireframeDesc, &m_WireframeRenderState);

	D3D11_RASTERIZER_DESC l_SolidDesc;
	ZeroMemory(&l_SolidDesc, sizeof(D3D11_RASTERIZER_DESC));
	l_SolidDesc.FillMode=D3D11_FILL_SOLID;
	l_SolidDesc.CullMode=D3D11_CULL_NONE;
	
	l_HR=m_Device->CreateRasterizerState(&l_SolidDesc, &m_SolidRenderState);
}

// Rendering
void CRenderManager::BeginRender()
{
	m_DeviceContext->ClearRenderTargetView(m_RenderTargetView, &m_BackgroundColor.x);
	m_DeviceContext->ClearDepthStencilView(m_DepthStencilView, D3D11_CLEAR_DEPTH, 1.0f, 0);
	SetSolidRenderState();
}

void CRenderManager::EndRender()
{
	m_SwapChain->Present(1, 0);
}

void CRenderManager::SetSolidRenderState()
{
	m_DeviceContext->RSSetState(m_SolidRenderState);
}

void Draw_Triangle()
{
	CDebugVertex resultBuffer[] = {
		{ Vect4f(-0.5f, -0.5f, 0.0f, 1.0f), Vect4f(1.0f, 0.0f, 0.0f, 1.0f) },
		{ Vect4f(+0.5f, -0.5f, 0.0f, 1.0f), Vect4f(0.0f, 1.0f, 0.0f, 1.0f) },
		{ Vect4f(+0.0f, +0.5f, 0.0f, 1.0f), Vect4f(0.0f, 0.0f, 1.0f, 1.0f) },
	};

	// set vertex data
	D3D11_MAPPED_SUBRESOURCE resource;
	HRESULT hr = m_DeviceContext->Map(m_DebugVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

	if (FAILED(hr))
		return; // TODO log

	assert(3 * sizeof(CDebugVertex) < DebugVertexBufferSize * sizeof(CDebugVertex));
	memcpy(resource.pData, resultBuffer, 3 * sizeof(CDebugVertex));

	m_DeviceContext->Unmap(m_DebugVertexBuffer, 0);


	UINT stride = sizeof(CDebugVertex);
	UINT offset = 0;
	m_DeviceContext->IASetVertexBuffers(0, 1, &m_DebugVertexBuffer, &stride, &offset);
	m_DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
	m_DeviceContext->IASetInputLayout(m_DebugVertexLayout);
	m_DeviceContext->VSSetShader(m_DebugVertexShader, NULL, 0);
	m_DeviceContext->PSSetShader(m_DebugPixelShader, NULL, 0);

	m_DeviceContext->Draw(3, 0);
}

void CRenderManager::DebugRender(const Mat44f& modelViewProj, const CDebugVertex* modelVertices, int numVertices, CColor colorTint)
{
	CDebugVertex *resultBuffer = (CDebugVertex *)alloca(numVertices * sizeof(CDebugVertex));

	for (int i = 0; i < numVertices; ++i)
	{
		resultBuffer[i].Position = (modelVertices[i].Position * modelViewProj);

		resultBuffer[i].Color = modelVertices[i].Color * colorTint;
	}

	// set vertex data
	D3D11_MAPPED_SUBRESOURCE resource;
	HRESULT hr = m_DeviceContext->Map(m_DebugVertexBuffer, 0, D3D11_MAP_WRITE_DISCARD, 0, &resource);

	if (FAILED(hr))
		return; // TODO log

	assert(numVertices * sizeof(CDebugVertex) < DebugVertexBufferSize * sizeof(CDebugVertex));
	memcpy(resource.pData, resultBuffer, numVertices * sizeof(CDebugVertex));

	m_DeviceContext->Unmap(m_DebugVertexBuffer, 0);


	UINT stride = sizeof(CDebugVertex);
	UINT offset = 0;
	m_DeviceContext->IASetVertexBuffers(0, 1, &m_DebugVertexBuffer, &stride, &offset);
	m_DeviceContext->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_LINELIST);
	m_DeviceContext->IASetInputLayout(m_DebugVertexLayout);
	m_DeviceContext->VSSetShader(m_DebugVertexShader, NULL, 0);
	m_DeviceContext->PSSetShader(m_DebugPixelShader, NULL, 0);

	m_DeviceContext->Draw(numVertices, 0);
}

void CRenderManager::DrawAxis(float SizeX, float SizeY, float SizeZ, const CColor &Color)
{
	Mat44f scale;
	scale.SetIdentity();
	scale.SetScale(SizeX, SizeY, SizeZ);


	Mat44f viewProj = scale * m_ModelViewProjectionMatrix;

	DebugRender(viewProj, m_AxisRenderableVertexs, m_NumVerticesAxis, Color);
}




































