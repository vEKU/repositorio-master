#include "Engine\Materials\MaterialManager.h"
#include "Base\Logger\Logger.h"


CMaterialManager::CMaterialManager() {

}
CMaterialManager::~CMaterialManager() {

}

bool CMaterialManager::Load(const std::string &Filename, const std::string &DefaultsFileName) {
	m_LevelMaterialsFilename = Filename;
	m_DefaultMaterialsFilename = DefaultsFileName;
	return (LoadMaterialsFromFile(DefaultsFileName, false) && LoadMaterialsFromFile(Filename, false));

}


bool CMaterialManager::Reload() {
	return true;
}

bool CMaterialManager::Save() {
	return true;
}

bool CMaterialManager::LoadMaterialsFromFile(const std::string &Filename, bool Update) {

	base::utils::CLogger().AddNewLog(base::utils::CLogger::ELogLevel::eLogInfo, "Load File");

	tinyxml2::XMLDocument document;
	tinyxml2::XMLError error = document.LoadFile(Filename.c_str());

	if (base::xml::SucceedLoad(error)) {
		base::utils::CLogger().AddNewLog(base::utils::CLogger::ELogLevel::eLogInfo, "Load XML OK");
		tinyxml2::XMLElement *materials = document.FirstChildElement("materials"); //Materials
		if (materials) {
			for (tinyxml2::XMLElement *material = materials->FirstChildElement("material"); material != nullptr; material = material->NextSiblingElement("material")) {//Material
				if (Update || !Exist(material->Attribute("name"))) {
					if (Exist(material->Attribute("name"))) {
						base::utils::CLogger().AddNewLog(base::utils::CLogger::ELogLevel::eLogInfo, "Actualizado");
					}
					CMaterial *material_to_save = new CMaterial(material);
					base::utils::CLogger().AddNewLog(base::utils::CLogger::ELogLevel::eLogInfo, "Nuevo material creado");
					base::utils::CTemplatedMap<CMaterial>::Update(material->Attribute("name"), material_to_save);
					base::utils::CLogger().AddNewLog(base::utils::CLogger::ELogLevel::eLogInfo, "Nuevo material a�adido");
				}
			}
		}
	}
	else {
		//TODO LOGGER
	}

	return base::xml::SucceedLoad(error);
}