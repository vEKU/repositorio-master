#ifndef TEMPLATED_MATERIAL_PARAMETER
#define TEMPLATED_MATERIAL_PARAMETER

#include "Base\Utils\Name.h"
#include "Base\Utils\Types.h"
#include "Engine\Materials\MaterialParameter.h"
#include "Engine\Materials\Material.h"

template<typename T> class CTemplatedMaterialParameter : public CMaterialParameter
{
public:
	CTemplatedMaterialParameter(const std::string& aName, const T &Value,
		CMaterial::TParameterType aType)
		: CMaterialParameter(aName, aType)
		, m_Value(Value)
	{}
	virtual ~CTemplatedMaterialParameter(){};
	virtual uint32 GetSize() const { return sizeof(T); };
	void * GetAddr(int index = 0) const;
	GET_SET_REF(T, Value);
private:
	T m_Value;
};

template<typename T> void* CTemplatedMaterialParameter<T>::GetAddr(int index = 0) const
{
	return (void*)((float*)&m_Value + index);

}

#endif