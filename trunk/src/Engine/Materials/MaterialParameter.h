#ifndef MATERIAL_PARAMETER_H
#define MATERIAL_PARAMETER_H

#include <stdint.h>
#include "Base\Utils\Name.h"
#include "Engine\Materials\Material.h"

class CMaterialParameter : public CName
{
public:
	CMaterialParameter(const std::string& aName, CMaterial::TParameterType aType);
	virtual ~CMaterialParameter();
	virtual uint32_t GetSize() const = 0;
	virtual void * GetAddr(int index = 0) const = 0;
	GET_SET_REF(std::string, Description)
	GET_SET(CMaterial::TParameterType, Type)

protected:
	std::string m_Description;
	CMaterial::TParameterType m_Type;
private:
	DISALLOW_COPY_AND_ASSIGN(CMaterialParameter);
};

#endif