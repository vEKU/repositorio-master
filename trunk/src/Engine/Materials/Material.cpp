#include "Engine\Materials\Material.h"
#include "Engine\Materials\TemplatedMaterialParameter.h"

#include "Base\Logger\Logger.h"
#include "Base\Math\Vector2.h"
#include "Base\Math\Vector3.h"
#include "Base\Math\Vector4.h"
#include "Base\Math\Color.h"


CMaterial::CMaterial(const CXMLElement* aElement) {
	for (const tinyxml2::XMLElement *texture = aElement->FirstChildElement("texture"); texture != nullptr; texture = texture->NextSiblingElement("texture")) {
		//cristian
	}
	for (const tinyxml2::XMLElement *parameter = aElement->FirstChildElement("parameter"); parameter != nullptr; parameter = parameter->NextSiblingElement("parameter")) {
		TParameterType lType;
		if (EnumString<TParameterType>::ToEnum(lType, parameter->GetAttribute<std::string>("type", "")))
		{
			CMaterialParameter* lParameter = nullptr;

			//MACRO
		#define CASE_CREATE_MATERIAL_PARAMETER( parameter_type, parameter_enum, parameter_def_value )\
		case parameter_enum: \
					{ \
			lParameter = new CTemplatedMaterialParameter<parameter_type> \
			( \
				parameter->GetAttribute<std::string>("name", ""), \
				parameter->GetAttribute<parameter_type>("value", parameter_def_value), \
				parameter_enum \
			); \
					}\
		break;

		switch (lType)
		{
			CASE_CREATE_MATERIAL_PARAMETER(float, eFloat, 0.0f)
			CASE_CREATE_MATERIAL_PARAMETER(Vect2f, eFloat2, Vect2f())
			CASE_CREATE_MATERIAL_PARAMETER(Vect3f, eFloat3, Vect3f())
			CASE_CREATE_MATERIAL_PARAMETER(Vect4f, eFloat4, Vect4f())
			CASE_CREATE_MATERIAL_PARAMETER(CColor, eColor, CColor())
		}
		#undef CASE_CREATE_MATERIAL_PARAMETER
			//END MACRO

			if (lParameter)
				mParameters.push_back(lParameter);

		}
	}
}
CMaterial::~CMaterial() {

}

void CMaterial::Apply() {

}

void CMaterial::ActivateParameters() {

}

void CMaterial::ActivateTextures() {

}