#include "Engine\Materials\MaterialParameter.h"

CMaterialParameter::CMaterialParameter(const std::string& aName, CMaterial::TParameterType aType) {
	SetDescription(aName);
	SetType(aType);
}

CMaterialParameter::~CMaterialParameter() {

}