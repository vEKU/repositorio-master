#ifndef MATERIAL_MANAGER_H
#define MATERIAL_MANAGER_H

#include "Base\Utils\TemplatedMap.h"
#include "Base\XML\tinyxml2\tinyxml2.h"
#include "Base\XML\XML.h"
#include "Engine\Materials\Material.h"


class CMaterialManager : public base::utils::CTemplatedMap<CMaterial> {

public:
	CMaterialManager();
	virtual ~CMaterialManager();
	bool Load(const std::string &Filename, const std::string &DefaultsFileName = "Material-Standard-Multi");
	bool Reload();
	bool Save();
private:
	std::string m_LevelMaterialsFilename;
	std::string m_DefaultMaterialsFilename;
	bool LoadMaterialsFromFile(const std::string &Filename, bool Update = false);
};
#endif