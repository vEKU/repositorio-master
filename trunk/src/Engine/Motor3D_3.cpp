
extern LRESULT ImGui_ImplDX11_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);


void main()
{
	while( msg.message != WM_QUIT )
	{
		l_InputManager.PreUpdate(s_WindowActive);

		while( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
		{
			ImGui_ImplDX11_WndProcHandler(hWnd, msg.message, msg.wParam, msg.lParam);
		}
	}
	{
		l_InputManager.PostUpdate();

		ImGui_ImplDX11_NewFrame();

		l_Engine.ProcessInputs();
		
		l_Engine.Update();
		l_Engine.Render();


    }
}

// camera controller

class CCameraController
{
public:
	CCameraController() :m_Position(0, 0, 0), m_Front(0, 0, 1), m_Up(0, 1, 0) {}
	virtual ~CCameraController() {}
	virtual void Update(float ElapsedTime) = 0;

	void SetToRenderManager(CRenderManager &_RenderManager) const
	{ _RenderManager.SetViewMatrix(m_Position, m_Position + m_Front, m_Up); }

protected:
	Vect3f 					m_Position;
	Vect3f 					m_Front;
	Vect3f 					m_Up;
};

class SphericalCameraController : public CCameraController
{
public:
	Vect3f center;
	float yawSpeed, pitchSpeed, rollSpeed;
	float zoomSpeed;

	SphericalCameraController(Vect3f center = Vect3f(0, 0, 0), float maxPitch = 1.5f, float minPitch = -1.5f, float maxZoom = 20, float minZoom = 1);
	virtual ~SphericalCameraController();

	virtual void Update(float ElapsedTime) override;

private:
	float yaw, pitch, roll, zoom;

	float maxPitch, minPitch;
	float maxZoom, minZoom;
};

void SphericalCameraController::Update(float ElapsedTime)
{
	yaw += yawSpeed * ElapsedTime;
	pitch += pitchSpeed * ElapsedTime;
	roll += rollSpeed * ElapsedTime;
	zoom += zoomSpeed * ElapsedTime;

	if (pitch > maxPitch)
		pitch = maxPitch;
	if (pitch < minPitch)
		pitch = minPitch;

	if (zoom > maxZoom)
		zoom = maxZoom;
	if (zoom < minZoom)
		zoom = minZoom;

	if (yaw > mathUtils::PiTimes<float>())
		yaw -= mathUtils::PiTimes<float>(2.0f);
	if (yaw < -mathUtils::PiTimes<float>())
		yaw += mathUtils::PiTimes<float>(2.0f);

	if (roll > mathUtils::PiTimes<float>())
		roll -= mathUtils::PiTimes<float>(2.0f);
	if (roll < -mathUtils::PiTimes<float>())
		roll += mathUtils::PiTimes<float>(2.0f);

	// set CameraControllerData
	m_Front.x = sin(yaw) * cos(-pitch);
	m_Front.y = -sin(-pitch);
	m_Front.z = -cos(yaw) * cos(-pitch);

	// TODO roll

	m_Position = center - m_Front * zoom;
}


// engine --------------------------------------------------------------------------------------------------------------


class CRenderManager;
class CActionManager;
class CCameraController;
class CMaterialManager;
class CSceneManager;

#define BUILD_GET_SET_ENGINE_MANAGER( Manager ) \
private: \
C##Manager* m_##Manager; \
public: \
void Set##Manager(C##Manager* a##Manager) { m_##Manager = a##Manager;  } \
const C##Manager& Set##Manager() const { return *m_##Manager; } \
C##Manager& Get##Manager() { return *m_##Manager; } \

class CEngine : public base::utils::CSingleton<CEngine>
{
public:

  CEngine();
  virtual ~CEngine();

  void ProcessInputs();
  void Update();
  void Render();

  BUILD_GET_SET_ENGINE_MANAGER(MaterialManager);
  BUILD_GET_SET_ENGINE_MANAGER(RenderManager);
  BUILD_GET_SET_ENGINE_MANAGER(CameraController);
  BUILD_GET_SET_ENGINE_MANAGER(SceneManager);
  BUILD_GET_SET_ENGINE_MANAGER(ActionManager);

private:

	std::chrono::monotonic_clock m_Clock;
	std::chrono::monotonic_clock::time_point m_PrevTime;
};

#undef BUILD_GET_SET_ENGINE_MANAGER


CEngine::CEngine()
	: m_RenderManager(nullptr)
	, m_ActionManager(nullptr)
	, m_CameraController(nullptr)
	, m_Clock()
	, m_PrevTime(m_Clock.now())
{
}


void CEngine::Update()
{
	auto currentTime = m_Clock.now();
	std::chrono::duration<float> chronoDeltaTime = currentTime - m_PrevTime;
	m_PrevTime = currentTime;

	float dt = chronoDeltaTime.count() > 0.5f ? 0.5f : chronoDeltaTime.count();

	// -----------------------------

	// game logic
	
	// -----------------------------

	m_CameraController->Update(dt);

	m_CameraController->SetToRenderManager(*m_RenderManager);
}


void CEngine::Render()
{
	m_RenderManager->BeginRender();

	// -----------------------------

	// game render

	// -----------------------------


	ImGui::Render();
	m_RenderManager->EndRender();
}

// -------------------------------------------------------------------------------------

void Camera_Test()
{
	// initialization
	CTPSCamera camera;
	camera.position = {0,0,0};
	camera.offset = {2, 1, -3};
	
	Engine.SetCameraController(&camera);

	// game logic
	camera.moveFWD = actionManager["FWD"].value;
	camera.moveRight = actionManager["STR"].value;
	
	camera.moveYaw = actionManager["Yaw"].value;
	camera.movePitch = actionManager["Pitch"].value;
	
	// game render
	Mat44 world;
	world.SetIdentity();
	renderManager.SetModelMatrix(world);
	renderManager.DrawGrid(10);
	
	
	world.SetIdentity();
	world.Translate(camera.position);
	renderManager.SetModelMatrix(world);
	renderManager.DrawSphere(1);
}
