#include <Input/ActionManager.h>

CActionManager::CActionManager(const HWND &hWnd)
{
	m_InputManager = CInputManager(hWnd);
}

CActionManager::CActionManager()
{
}
CActionManager::~CActionManager()
{
}

ActionTrigger::TriggerType ActionTrigger::GetTriggerTypeFromString(const char* str, TriggerType defaultValue)
{
	if (str == nullptr)
		return defaultValue;
	else if (strcmp(str, "KEYBOARD") == 0)
		return TriggerType::KEYBOARD;
	else if (strcmp(str, "MOUSE") == 0)
		return TriggerType::MOUSE;
	else if (strcmp(str, "MOUSE_BUTTON") == 0)
		return TriggerType::MOUSE_BUTTON;
	else if (strcmp(str, "GAMEPAD") == 0)
		return TriggerType::GAMEPAD;
	else if (strcmp(str, "GAMEPAD_BUTTON") == 0)
		return TriggerType::GAMEPAD_BUTTON;
	else
		return defaultValue; // (TriggerType)-1;
}

ActionTrigger::ButtonActionType ActionTrigger::GetButtonActionTypeFromString(const char* str, ButtonActionType defaultValue) {
	if (str == nullptr)
		return defaultValue;
	else if (strcmp(str, "BECOMES_PRESSED") == 0) {
		return ButtonActionType::BecomesPressed;
	}
	else if (strcmp(str, "BECOMES_RELEASED") == 0) {
		return ButtonActionType::BecomesReleased;
	}
	else if (strcmp(str, "IS_PRESSED") == 0) {
		return ButtonActionType::IsPressed;
	}
	else if (strcmp(str, "IS_RELEASED") == 0) {
		return ButtonActionType::IsReleased;
	}
	else {
		return defaultValue;
	}
}

float ActionTrigger::GetTresholdFromString(const char* str, float defaultValue) {
	return 0;
}

bool ActionTrigger::GetGeTresholdFromString(const char* str, bool defaultValue) {
	if ((strcmp(str, "true") == 0) || (strcmp(str, "TRUE") == 0)) {
		return true;
	} else {
		return false;
	}
}


void CActionManager::SetDemoActions()
{
	{
		InputAction action = {};

		ActionTrigger trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'A';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;

		action.triggers.push_back(trigger);

		base::utils::CTemplatedMap<InputAction>::Update("RotateLeft", new InputAction(action));
	}
	{
		InputAction action = {};

		ActionTrigger trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'D';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;

		action.triggers.push_back(trigger);

		base::utils::CTemplatedMap<InputAction>::Update("RotateRight", new InputAction(action));
	}
	{
		InputAction action = {};

		ActionTrigger trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'W';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;
		trigger.keyboard.value = 1;

		action.triggers.push_back(trigger);

		trigger = {};
		trigger.type = ActionTrigger::KEYBOARD;
		trigger.keyboard.keyCode = 'S';
		trigger.keyboard.actionType = ActionTrigger::IsPressed;
		trigger.keyboard.value = -1;

		action.triggers.push_back(trigger);

		trigger = {};
		trigger.type = ActionTrigger::MOUSE_BUTTON;
		trigger.mouseButton.button = InputDefinitions::LEFT;
		trigger.mouseButton.actionType = ActionTrigger::IsPressed;
		trigger.mouseButton.value = 1;

		action.triggers.push_back(trigger);

		trigger = {};
		trigger.type = ActionTrigger::MOUSE_BUTTON;
		trigger.mouseButton.button = InputDefinitions::RIGHT;
		trigger.mouseButton.actionType = ActionTrigger::IsPressed;
		trigger.mouseButton.value = -1;

		action.triggers.push_back(trigger);

		base::utils::CTemplatedMap<InputAction>::Update("Move", new InputAction(action));
	}
}

void CActionManager::Update() {
	for (auto &actionIt : m_ResourcesMap)
	{
		assert(actionIt.second != NULL);//DEBUG

		InputAction *action = actionIt.second;
		action->active = false;
		action->value = 0;

		for (ActionTrigger& trigger : action->triggers)
		{
			switch (trigger.type)
			{
			case ActionTrigger::KEYBOARD:
				switch (trigger.keyboard.actionType)
				{
				case ActionTrigger::IsPressed:
					if (m_InputManager.IsKeyPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::IsReleased:
					if (!m_InputManager.IsKeyPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::BecomesPressed:
					if (m_InputManager.KeyBecomesPressed(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				case ActionTrigger::BecomesReleased:
					if (m_InputManager.KeyBecomesReleased(trigger.keyboard.keyCode))
					{
						action->active = true;
						action->value = trigger.keyboard.value;
					}
					break;
				default:
					//assert(false);
					break;
				}
				break;
			case ActionTrigger::MOUSE:
			{
				if (action->value == 0.0f)
				{
					float value = (float)m_InputManager.GetMouseAxis(trigger.mouse.axis);
					float comparingValue = value;
					if (comparingValue < 0) comparingValue *= -1.0f;
					if (value != 0.0f)
					{
						value = value;
					}
					bool active = (trigger.mouse.geThreshold) ?
						(comparingValue >= trigger.mouse.threshold) : (comparingValue < trigger.mouse.threshold);
					if (fabs(value) > fabs(action->value))
					{
						if (active || !action->active)
						{
							action->value = value;
							action->active = active;
						}
					}
				}
				break;
			}
			case ActionTrigger::MOUSE_BUTTON:
				switch (trigger.mouseButton.actionType)
				{
				case ActionTrigger::IsPressed:
					if (m_InputManager.IsMouseButtonPressed(trigger.mouseButton.button))
					{
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
				break;
				case ActionTrigger::IsReleased:
					if (!m_InputManager.IsMouseButtonPressed(trigger.mouseButton.button)) {
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				case ActionTrigger::BecomesPressed:
					if (!m_InputManager.MouseButtonBecomesPressed(trigger.mouseButton.button)) {
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				case ActionTrigger::BecomesReleased:
					if (!m_InputManager.MouseButtonBecomesReleased(trigger.mouseButton.button)) {
						action->active = true;
						action->value = trigger.mouseButton.value;
					}
					break;
				default:
					//assert(false);
					break;
				}
				break;
			case ActionTrigger::GAMEPAD:
				if (action->value == 0.0f && m_InputManager.HasGamepad())
				{
					action->value = m_InputManager.GetGamepadAxis(trigger.gamepad.axis);
					if (action->value != 0.0f) {
						action->active = true;
					}
				}
				break;
			case ActionTrigger::GAMEPAD_BUTTON:
				// TODO
				break;
			default:
				//assert(false);
				break;
			}
		}
	}
}

inline unsigned char CActionManager::GetKeyCode(const std::string& str)
{
	if (str.length() == 1 && ((str[0] >= 'A' && str[0] <= 'Z') || (str[0] >= '0' && str[0] <= '9')))
	{
		return str[0];
	}
	else if (str == "SPACE" || str == " ")
	{
		return VK_SPACE;
	}
	else if (str == "ARROW_RIGHT")
	{
		return VK_RIGHT;
	}
	else if (str == "ARROW_LEFT")
	{
		return VK_LEFT;
	}
	else if (str == "ARROW_UP")
	{
		return VK_UP;
	}
	else if (str == "ARROW_DOWN")
	{
		return VK_DOWN;
	}
	else
	{
		assert(false);
		return -1;
	}
}

bool CActionManager::LoadActions(const std::string &path)
{
	bool ok = false;

	tinyxml2::XMLDocument document;
	tinyxml2::XMLError error = document.LoadFile(path.c_str());
	if (base::xml::SucceedLoad(error))
	{
		tinyxml2::XMLElement *actions = document.FirstChildElement("actions");
		if (actions)
		{
			for (tinyxml2::XMLElement *action = actions->FirstChildElement(); action != nullptr; action = action->NextSiblingElement())
			{
				if (strcmp(action->Name(), "action") == 0)
				{
					InputAction inputAction = {};
					const char *actionName = action->Attribute("name");
					assert(actionName != nullptr);

					for (tinyxml2::XMLElement *trigger = action->FirstChildElement(); trigger != nullptr; trigger = trigger->NextSiblingElement())
					{
						if (strcmp(trigger->Name(), "trigger") == 0)
						{
							ActionTrigger actionTrigger = {};

							actionTrigger.type = ActionTrigger::GetTriggerTypeFromString(trigger->Attribute("type"));
							switch (actionTrigger.type)
							{
							case ActionTrigger::KEYBOARD:
								actionTrigger.keyboard.keyCode = GetKeyCode(trigger->Attribute("key_code"));
								actionTrigger.keyboard.actionType = ActionTrigger::GetButtonActionTypeFromString(trigger->Attribute("button_type"), ActionTrigger::IsPressed);
								actionTrigger.keyboard.value = trigger->FloatAttribute("value");
								break;
							case ActionTrigger::MOUSE:
								actionTrigger.mouse.axis = InputDefinitions::GetMouseAxisFromString(trigger->Attribute("axis"));
								actionTrigger.mouse.threshold = ActionTrigger::GetTresholdFromString(trigger->Attribute("treshold"));
								actionTrigger.mouse.geThreshold = ActionTrigger::GetGeTresholdFromString(trigger->Attribute("ge_treshold"));
	
								break;
							case ActionTrigger::MOUSE_BUTTON:
								actionTrigger.mouseButton.button = InputDefinitions::GetMouseButtonFromString(trigger->Attribute("button"), InputDefinitions::LEFT);
								actionTrigger.mouseButton.actionType = ActionTrigger::GetButtonActionTypeFromString(trigger->Attribute("button_type"), ActionTrigger::IsPressed);
								actionTrigger.mouseButton.value = trigger->FloatAttribute("value");
								break;
							case ActionTrigger::GAMEPAD:
								actionTrigger.gamepad.axis = InputDefinitions::GetGamepadAxisFromString(trigger->Attribute("axis"));
								break;
							case ActionTrigger::GAMEPAD_BUTTON:
							default:
								assert(false);
								break;
							}

							inputAction.triggers.push_back(actionTrigger);
						}
						else
						{
							assert(false); // TODO better log error
						}
					}


					base::utils::CTemplatedMap<InputAction>::Update(actionName, new InputAction(inputAction));
				}
				else
				{
					assert(false); // TODO better log error
				}
			}
		}
		ok = true;
	}
	return ok;
}

void CActionManager::PreUpdate(bool isWindowActive) {
	m_InputManager.PreUpdate(isWindowActive);
}

void CActionManager::PostUpdate() {
	m_InputManager.PostUpdate();
}



bool CActionManager::HandleMouse(const MSG& msg) {
	return m_InputManager.HandleMouse(msg);
}

bool CActionManager::HandleKeyboard(const MSG& msg){
	return m_InputManager.HandleKeyboard(msg);
}
