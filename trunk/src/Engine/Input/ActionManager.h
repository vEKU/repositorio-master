#ifndef ACTION_MANAGER
#define ACTION_MANAGER


#include <Windows.h>
#include <vector>
#include <string>
#include <Base\Utils\TemplatedMap.h>
#include <Engine\Input\InputManager.h>
#include "XML\tinyxml2\tinyxml2.h"
#include "XML\XML.h"
#include "Base\DearImGUI\imgui.h"

#include <assert.h>

struct ActionTrigger
{
	enum ButtonActionType
	{
		IsPressed, IsReleased, BecomesPressed, BecomesReleased
	};
	enum TriggerType {
		KEYBOARD,
		MOUSE,
		MOUSE_BUTTON,
		GAMEPAD,
		GAMEPAD_BUTTON
	} type;
	union
	{
		struct
		{
			ButtonActionType actionType;
			unsigned char keyCode;
			float value;
		} keyboard;
		struct MouseButton
		{
			ButtonActionType actionType;
			InputDefinitions::MouseButton button;
			float value;
		} mouseButton;
		struct
		{
			ButtonActionType actionType;
			InputDefinitions::GamepadButton button;
			float value;
		} gamepadButton;

		struct
		{
			float threshold;
			InputDefinitions::MouseAxis axis;
			bool geThreshold;
		} mouse;
		struct
		{
			float threshold;
			InputDefinitions::GamepadAxis axis;
			bool geThreshold;
		} gamepad;
	};


	static TriggerType GetTriggerTypeFromString(const char* str, TriggerType defaultValue = (TriggerType)-1);
	static ButtonActionType GetButtonActionTypeFromString(const char* str, ButtonActionType defaultValue = (ButtonActionType)-1);
	static float GetTresholdFromString(const char* str, float defaultValue = 0.0f);
	static bool GetGeTresholdFromString(const char* str, bool defaultValue = false);
};


struct InputAction{
	float value;
	bool active;

	std::vector<ActionTrigger> triggers;
};

class CActionManager : public base::utils::CTemplatedMap<InputAction>
{
public:

	CActionManager(const HWND &hWnd);
	CActionManager();
	~CActionManager();

	bool LoadActions(const std::string &path);
	void SetDemoActions();
	void Update();

	void PreUpdate(bool isWindowActive);
	void PostUpdate();
	bool HandleMouse(const MSG& msg);
	bool HandleKeyboard(const MSG& msg);

	unsigned char GetKeyCode(const std::string& str);

private:
 CInputManager m_InputManager;


};
#endif