#ifndef INPUT_MANAGER
#define INPUT_MANAGER


#include <Windows.h>
#include "InputDefinitions.h"
//#include "Engine\Input\GamepadController.h"


class CInputManager {
public:
	CInputManager(const HWND &hWnd);
	CInputManager();
	~CInputManager();

	void Init(const HWND &hWnd);

	bool HandleKeyboard(const MSG& msg);
	bool HandleMouse(const MSG& msg);

	void PreUpdate(bool isWindowActive);
	void PostUpdate();

	bool IsKeyPressed(unsigned char KeyCode) const { return m_KeyboardState.raw[KeyCode]; }
	bool KeyBecomesPressed(unsigned char KeyCode) const { return m_KeyboardState.raw[KeyCode] && !m_PreviousKeyboardState.raw[KeyCode]; }
	bool KeyBecomesReleased(unsigned char KeyCode) const { return !m_KeyboardState.raw[KeyCode] && m_PreviousKeyboardState.raw[KeyCode]; }

	int GetMouseMovementX() const { return m_MouseMovementX; }
	int GetMouseMovementY() const { return m_MouseMovementY; }
	int GetMouseMovementZ() const { return m_MouseMovementZ; }

	int GetMouseAxis(InputDefinitions::MouseAxis axis) const;

	int GetMouseX() const { return m_MouseX; }
	int GetMouseY() const { return m_MouseY; }


	bool IsLeftMouseButtonPressed() const { return m_ButtonLeft; }
	bool IsMiddleMouseButtonPressed() const { return m_ButtonMiddle; }
	bool IsRightMouseButtonPressed() const { return m_ButtonRight; }


	bool LeftMouseButtonBecomesPressed() const { return m_ButtonLeft && !m_PreviousButtonLeft; }
	bool MiddleMouseButtonBecomesPressed() const { return m_ButtonMiddle && !m_PreviousButtonMiddle; }
	bool RightMouseButtonBecomesPressed() const { return m_ButtonRight && !m_PreviousButtonRight; }

	bool LeftMouseButtonBecomesReleased() const { return !m_ButtonLeft && m_PreviousButtonLeft; }
	bool MiddleMouseButtonBecomesReleased() const { return !m_ButtonMiddle && m_PreviousButtonMiddle; }
	bool RightMouseButtonBecomesReleased() const { return !m_ButtonRight && m_PreviousButtonRight; }


	bool IsMouseButtonPressed(InputDefinitions::MouseButton button) const;
	bool MouseButtonBecomesPressed(InputDefinitions::MouseButton button) const;
	bool MouseButtonBecomesReleased(InputDefinitions::MouseButton button) const;

	//GAMEPAD

	bool HasGamepad() { return m_HasGamepad; }
	float ProcessXInputStickValue(SHORT Value, SHORT DeadZoneTreshHold);
	float ProcessXInputTriggerValue(SHORT Value, SHORT DeadZoneTreshHold);

	float GetGamepadAxis(InputDefinitions::GamepadAxis);

	float GetGamepadStickLeftX() const { return m_GamepadStickLeftX; }
	float GetGamepadStickLeftY() const { return m_GamepadStickLeftY; }
	float GetGamepadStickRightX() const { return m_GamepadStickRightX; }
	float GetGamepadStickRightY() const { return m_GamepadStickRightY; }
	float GetGamepadTriggerRight() const { return m_GamepadTriggerRight; }
	float GetGamepadTriggerLeft() const { return m_GamepadTriggerLeft; }

	

private:

	struct KeyboardData
	{
		bool raw[256];
		bool escape, space;
		bool numpad[10];
		bool fkey[24];
		bool left, right, up, down;

		void SetKey(unsigned char key, bool state);

	} m_PreviousKeyboardState, m_KeyboardState;

	int									m_MouseMovementX;
	int									m_MouseMovementY;
	int									m_MouseMovementZ;
	int									m_MouseX;
	int									m_MouseY;
	bool								m_ButtonRight, m_PreviousButtonRight;
	bool								m_ButtonLeft, m_PreviousButtonLeft;
	bool								m_ButtonMiddle, m_PreviousButtonMiddle;

	bool								m_HasGamepad;
	float								m_GamepadStickLeftX, m_GamepadStickLeftY;
	float								m_GamepadStickRightX, m_GamepadStickRightY;
	float								m_GamepadTriggerRight, m_GamepadTriggerLeft;
	bool								m_GamepadA, m_PreviousGamepadA;

	bool m_HighPrecisionMouseRegistered;


};
#endif