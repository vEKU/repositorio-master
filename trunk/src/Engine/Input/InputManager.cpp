#include <Input/InputManager.h>
#include <Windows.h>
#include <Base\DearImGUI\imgui.h>
#include "Engine\Input\GamepadController.h"

CInputManager::CInputManager(const HWND &hWnd) {
	Init(hWnd);

	HMODULE XInputLibrary = LoadLibraryA("xinput1_4.dll");
	if (!XInputLibrary) 
	{
		XInputLibrary = LoadLibraryA("xinput9_1_0.dll");
	}
	if (!XInputLibrary)
	{
		XInputLibrary = LoadLibraryA("xinput1_3.dll");
	}
	if (XInputLibrary)
	{
		InputGetState = (TInputGetState*)GetProcAddress(XInputLibrary, "XInputGetState");
		if (!InputGetState)
		{
			InputGetState = InputGetStateStub;
		}
		InputSetState = (TInputSetState*)GetProcAddress(XInputLibrary, "XInputSetState");
		if (!InputSetState)
		{
			InputSetState = InputSetStateStub;
		}
	}
}


CInputManager::CInputManager() {
}

CInputManager::~CInputManager() {

}

void CInputManager::Init(const HWND &hWnd) {
	{
		RAWINPUTDEVICE Rid[1];
		Rid[0].usUsagePage = 0x01;
		Rid[0].usUsage = 0x02;
		Rid[0].dwFlags = RIDEV_INPUTSINK;
		Rid[0].hwndTarget = hWnd;
		m_HighPrecisionMouseRegistered = (RegisterRawInputDevices(Rid, 1, sizeof(Rid[0])) != 0);
	}
	m_KeyboardState = {};
	m_ButtonLeft = m_ButtonMiddle = m_ButtonRight = false;
}


void CInputManager::PreUpdate(bool isWindowActive)
{
	m_PreviousKeyboardState = m_KeyboardState;

	m_PreviousButtonLeft = m_ButtonLeft;
	m_PreviousButtonMiddle = m_ButtonMiddle;
	m_PreviousButtonRight = m_ButtonRight;

	m_MouseMovementX = m_MouseMovementY = m_MouseMovementZ = 0;
	m_MouseX = m_MouseY = 0;
	
	m_PreviousGamepadA = m_GamepadA;

	if (!isWindowActive)
	{
		m_KeyboardState = {};

		m_ButtonLeft = m_ButtonMiddle = m_ButtonRight = false;
	}
}

void CInputManager::PostUpdate()
{
	XINPUT_STATE l_ControllerState;
	if (InputGetState(0, &l_ControllerState) == ERROR_SUCCESS)
	{
		m_HasGamepad = true;
		m_GamepadStickRightX = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbRX, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		m_GamepadStickRightY = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbRY, XINPUT_GAMEPAD_RIGHT_THUMB_DEADZONE);
		m_GamepadStickLeftX = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLX, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		m_GamepadStickLeftY = ProcessXInputStickValue(l_ControllerState.Gamepad.sThumbLY, XINPUT_GAMEPAD_LEFT_THUMB_DEADZONE);
		m_GamepadTriggerRight = ProcessXInputTriggerValue(l_ControllerState.Gamepad.bRightTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);
		m_GamepadTriggerLeft = ProcessXInputTriggerValue(l_ControllerState.Gamepad.bLeftTrigger, XINPUT_GAMEPAD_TRIGGER_THRESHOLD);

		m_GamepadA = (l_ControllerState.Gamepad.wButtons & XINPUT_GAMEPAD_A) != 0;
	}
	else
	{
		m_HasGamepad = false;
	}
}


bool CInputManager::HandleKeyboard(const MSG& msg)
{
	bool handled = false;

	unsigned char VKCode = (unsigned char)msg.wParam;

	int repeat = msg.lParam & 0xffff;
	bool AltKeyWasDown = ((msg.lParam & (1 << 29)) != 0);
	bool WasDown = ((msg.lParam & (1 << 30)) != 0);

	bool IsDown = ((msg.lParam & (1 << 31)) == 0);

	if (WasDown != IsDown)
	{
		m_KeyboardState.SetKey(VKCode, IsDown);
		handled = true;
	}
	return handled;
}

void CInputManager::KeyboardData::SetKey(unsigned char key, bool state)
{
	raw[key] = state;

	if (key >= VK_NUMPAD0 || key <= VK_NUMPAD9)
	{
		numpad[key - VK_NUMPAD0] = state;
	}
	else if (key >= VK_F1 || key <= VK_F24)
	{
		fkey[key - VK_F1] = state;
	}
	else
	{
		switch (key)
		{
		case VK_ESCAPE:
			escape = state;
			break;
		case VK_SPACE:
			space = state;
			break;
		case VK_LEFT:
			left = state;
			break;
		case VK_UP:
			up = state;
			break;
		case VK_RIGHT:
			right = state;
			break;
		case VK_DOWN:
			down = state;
			break;
		}
	}
}


bool CInputManager::HandleMouse(const MSG& msg)
{
	switch (msg.message)
	{
	case WM_LBUTTONDOWN:
		m_ButtonLeft = true;
		return true;
	case WM_MBUTTONDOWN:
		m_ButtonMiddle = true;
		return true;
	case WM_RBUTTONDOWN:
		m_ButtonRight = true;
		return true;
	case WM_LBUTTONUP:
		m_ButtonLeft = false;
		return true;
	case WM_MBUTTONUP:
		m_ButtonMiddle = false;
		return true;
	case WM_RBUTTONUP:
		m_ButtonRight = false;
		return true;
	case WM_MOUSEMOVE:
		m_MouseX = LOWORD(msg.lParam);
		m_MouseY = HIWORD(msg.lParam);
		return true;
	case WM_MOUSEWHEEL:
		m_MouseMovementZ = GET_WHEEL_DELTA_WPARAM(msg.wParam);
		return true;
	case WM_INPUT:
	{
		UINT dwSize = 60;
		static BYTE lpb[60];

		UINT read = GetRawInputData((HRAWINPUT)msg.lParam, RID_INPUT, lpb, &dwSize, sizeof(RAWINPUTHEADER));

		RAWINPUT* raw = (RAWINPUT*)lpb;

		if (raw->header.dwType == RIM_TYPEMOUSE && read <= dwSize)
		{
			m_MouseMovementX += raw->data.mouse.lLastX;
			m_MouseMovementY += raw->data.mouse.lLastY;

			return true;
		}
		return false;
	}
	default:
		return false;
	}
}

int CInputManager::GetMouseAxis(InputDefinitions::MouseAxis axis) const
{
	switch (axis)
	{
	case InputDefinitions::DX:
		return GetMouseMovementX();
	case InputDefinitions::DY:
		return GetMouseMovementY();
	case InputDefinitions::DZ:
		return GetMouseMovementZ();
	default:
		return 0;
	}
}

bool CInputManager::IsMouseButtonPressed(InputDefinitions::MouseButton button) const {
	switch (button) {
		case InputDefinitions::MouseButton::LEFT:
			return IsLeftMouseButtonPressed();
			break;
		case InputDefinitions::MouseButton::RIGHT:
			return IsRightMouseButtonPressed();
			break;
		case InputDefinitions::MouseButton::MIDDLE:
			return IsMiddleMouseButtonPressed();
			break;
		default:
			return false;
	}
}


bool CInputManager::MouseButtonBecomesPressed(InputDefinitions::MouseButton button) const {
	switch (button) {
	case InputDefinitions::MouseButton::LEFT:
		return LeftMouseButtonBecomesPressed();
		break;
	case InputDefinitions::MouseButton::RIGHT:
		return RightMouseButtonBecomesPressed();
		break;
	case InputDefinitions::MouseButton::MIDDLE:
		return MiddleMouseButtonBecomesPressed();
		break;
	default:
		return false;
	}
}

bool CInputManager::MouseButtonBecomesReleased(InputDefinitions::MouseButton button) const {
	switch (button) {
	case InputDefinitions::MouseButton::LEFT:
		return LeftMouseButtonBecomesReleased();
		break;
	case InputDefinitions::MouseButton::RIGHT:
		return RightMouseButtonBecomesReleased();
		break;
	case InputDefinitions::MouseButton::MIDDLE:
		return MiddleMouseButtonBecomesReleased();
		break;
	default:
		return false;
	}
}

//GAMEPAD

float CInputManager::ProcessXInputStickValue(SHORT Value, SHORT DeadZoneTreshHold) {
	float Result = 0;

	if (Value < -DeadZoneTreshHold) {
		Result = (float)((Value + DeadZoneTreshHold) / (32768.0f - DeadZoneTreshHold));
	}
	else if (Value > DeadZoneTreshHold)
	{
		Result = (float)((Value - DeadZoneTreshHold) / (32768.0f - DeadZoneTreshHold));
	}
	return Result;
}


float CInputManager::ProcessXInputTriggerValue(SHORT Value, SHORT DeadZoneTreshHold) {
	float Result = 0;
	
	if (Value > DeadZoneTreshHold)
	{
		Result = (float)((Value - DeadZoneTreshHold) / (255.0f - DeadZoneTreshHold));
	}

	return Result;
}


//GAMEPAD
float CInputManager::GetGamepadAxis(InputDefinitions::GamepadAxis axis) {
	switch (axis)
	{
	case InputDefinitions::GamepadAxis::LEFT_THUMB_X:
		return GetGamepadStickLeftX();
	case InputDefinitions::GamepadAxis::LEFT_THUMB_Y:
		return GetGamepadStickLeftY();
	case InputDefinitions::GamepadAxis::RIGHT_THUMB_X:
		return GetGamepadStickRightX();
	case InputDefinitions::GamepadAxis::RIGHT_THUMB_Y:
		return GetGamepadStickRightY();
	case InputDefinitions::GamepadAxis::RIGHT_TRIGGER:
		return GetGamepadTriggerRight();
	case InputDefinitions::GamepadAxis::LEFT_TRIGGER:
		return GetGamepadTriggerLeft();
	default:
		return 0;
	}
}