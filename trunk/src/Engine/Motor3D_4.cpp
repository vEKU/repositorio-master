
#include <memory>

template<typename T>
struct CReleaser
{
	void operator()(T* obj)
	{
		obj->Release();
	}
};

template<typename T>
using releaser_ptr = std::unique_ptr < T, CReleaser<T> >;

// ---------------------------------------------------------

void CRenderManager::Resize(int Width, int Height)
{
	if (m_Device.get() != nullptr)
	{
		m_DeviceContext->OMSetRenderTargets(0, nullptr, nullptr);

		m_RenderTargetView.reset(nullptr);
		m_DepthStencil.reset(nullptr);
		m_DepthStencilView.reset(nullptr);

		m_SwapChain->ResizeBuffers(0, Width, Height, DXGI_FORMAT_UNKNOWN, 0);
		CreateBackBuffers(Width, Height); // where we initialize m_RenderTargetView, m_DepthStencil, m_DepthStencilView
	}
}

LRESULT WINAPI MsgProc( HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam )
{
	

  switch( msg )
  {
  case WM_SIZE:
	  if (wParam != SIZE_MINIMIZED)
	  {
		  auto& renderManager = CEngine::GetInstance().GetRenderManager();
		  renderManager.Resize((UINT)LOWORD(lParam), (UINT)HIWORD(lParam));
	  }
	  return 0;
  }
}

// --------------------------------------------------------

void CRenderManager::ClearAltIntro()
{
	// treure el ALT+INTRO automàtic
	IDXGIFactory* dxgiFactory;
	hr = m_SwapChain->GetParent(__uuidof(IDXGIFactory), (void **)&dxgiFactory);
	assert(hr == S_OK);

	hr = dxgiFactory->MakeWindowAssociation(hWnd, DXGI_MWA_NO_ALT_ENTER);
	assert(hr == S_OK);

	dxgiFactory->Release();
}


void ToggleFullscreen(HWND Window, WINDOWPLACEMENT &WindowPosition)
{
	// This follows Raymond Chen's prescription
	// for fullscreen toggling, see:
	// http://blogs.msdn.com/b/oldnewthing/archive/2010/04/12/9994016.aspx

	DWORD Style = GetWindowLongW(Window, GWL_STYLE);
	if (Style & WS_OVERLAPPEDWINDOW)
	{
		MONITORINFO MonitorInfo = { sizeof(MonitorInfo) };
		if (GetWindowPlacement(Window, &WindowPosition) &&
			GetMonitorInfoW(MonitorFromWindow(Window, MONITOR_DEFAULTTOPRIMARY), &MonitorInfo))
		{
			SetWindowLongW(Window, GWL_STYLE, Style & ~WS_OVERLAPPEDWINDOW);
			SetWindowPos(Window, HWND_TOP,
				MonitorInfo.rcMonitor.left, MonitorInfo.rcMonitor.top,
				MonitorInfo.rcMonitor.right - MonitorInfo.rcMonitor.left,
				MonitorInfo.rcMonitor.bottom - MonitorInfo.rcMonitor.top,
				SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
		}
	}
	else
	{
		SetWindowLongW(Window, GWL_STYLE, Style | WS_OVERLAPPEDWINDOW);
		SetWindowPlacement(Window, &WindowPosition);
		SetWindowPos(Window, 0, 0, 0, 0, 0,
			SWP_NOMOVE | SWP_NOSIZE | SWP_NOZORDER |
			SWP_NOOWNERZORDER | SWP_FRAMECHANGED);
	}
}

void main()
{
	while( PeekMessage( &msg, NULL, 0U, 0U, PM_REMOVE ) )
	{
		ImGui_ImplDX11_WndProcHandler(hWnd, msg.message, msg.wParam, msg.lParam);

		bool fHandled = false;
		if (msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST)
		{
			bool WasDown = ((msg.lParam & (1 << 30)) != 0);
			bool IsDown = ((msg.lParam & (1 << 31)) == 0);
			bool Alt = ((msg.lParam & (1 << 29)) != 0);
                  
			if (!WasDown && IsDown && Alt && msg.wParam == VK_RETURN)
			{
				WINDOWPLACEMENT windowPosition = { sizeof(WINDOWPLACEMENT) };
				GetWindowPlacement(msg.hwnd, &windowPosition);

				ToggleFullscreen(msg.hwnd, windowPosition);
				fHandled = true;
			}
			else if (msg.message == WM_KEYDOWN && msg.wParam == VK_ESCAPE)
			{
				PostQuitMessage(0);
			}
			else
			{
				fHandled = l_InputManager.HandleKeyboard(msg);
			}
		}
	}
}
// --------------------------------------------------------

void InitWithDebug(bool debugD3D)
{
	if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, debugD3D ? D3D11_CREATE_DEVICE_DEBUG : 0, featureLevels, numFeatureLevels,
		D3D11_SDK_VERSION, &sd, &l_SwapChain, &l_Device, NULL, &l_DeviceContext)))
	{
		debugD3D = false;
		if (FAILED(D3D11CreateDeviceAndSwapChain(NULL, D3D_DRIVER_TYPE_HARDWARE, NULL, 0, featureLevels, numFeatureLevels,
			D3D11_SDK_VERSION, &sd, &l_SwapChain, &l_Device, NULL, &l_DeviceContext)))
		{
			return FALSE;
		}
	}
	m_Device.reset(l_Device);
	m_DeviceContext.reset(l_DeviceContext);
	m_SwapChain.reset(l_SwapChain);

	if (debugD3D)
	{
		ID3D11Debug *l_D3DDebug;
		hr = m_Device->QueryInterface(__uuidof(ID3D11Debug), reinterpret_cast<void**>(&l_D3DDebug));
		if (SUCCEEDED(hr))
		{
			debugD3D = false;
		}
		else
		{
			m_D3DDebug.reset(l_D3DDebug);
		}
	}
}