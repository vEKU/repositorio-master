#include <Windows.h>
#include <Utils\MemLeaks\MemLeaks.h>

#include "Engine\Engine.h"

#include "Engine\Render\RenderManager.h"
#include "Engine\Input\ActionManager.h"
#include "Engine\Materials\MaterialManager.h"

#include "Engine\Camera\SphericalCameraController.h"
#include "Engine\Camera\FPSCameraController.h"

#include "Base\DearImGUI\imgui.h"
#include "Engine\DearImGUI\imgui_impl_dx11.h"



#define APPLICATION_NAME	"VIDEOGAME"

extern LRESULT ImGui_ImplDX11_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);

bool s_WindowActive;

//Tama�o ventana
unsigned int Width = 1280;
unsigned int Height = 720;

//Update camera
const float elapsedTime = 0.016;

//Muestra informaci�n sobre Videogame
void displayImguiInfo() {
	ImGui::Begin("Videogame Info", false, ImGuiWindowFlags_AlwaysAutoResize);
	ImGui::LabelText("Screen size", "Width: %i Height: %i", Width, Height);
	ImGui::Text("FPS: %f", ImGui::GetIO().Framerate);
	ImGui::End();
}


//-----------------------------------------------------------------------------
// Name: MsgProc()
// Desc: The window's message handler
//-----------------------------------------------------------------------------
LRESULT WINAPI MsgProc(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam) 
{

	switch (msg)
	{
	case WM_DESTROY:
	{
		PostQuitMessage(0);
		return 0;
	}
	break;
	case WM_KEYDOWN:
	{
		switch (wParam)
		{
		case VK_ESCAPE:
			//Cleanup();
			PostQuitMessage(0);
			return 0;
			break;
		}
	}
	break;
	// on msgproc:
	case WM_ACTIVATE:
	{
		s_WindowActive = wParam != WA_INACTIVE; 
		break;
	}
	}//end switch( msg )

	return DefWindowProc(hWnd, msg, wParam, lParam);
}

//-----------------------------------------------------------------------
// WinMain
//-----------------------------------------------------------------------
int APIENTRY WinMain(HINSTANCE _hInstance, HINSTANCE _hPrevInstance, LPSTR _lpCmdLine, int _nCmdShow)
{

#ifdef _DEBUG
	MemLeaks::MemoryBegin();
#endif;

	// Register the window class
	WNDCLASSEX wc = { sizeof(WNDCLASSEX), CS_CLASSDC, MsgProc, 0L, 0L, GetModuleHandle(NULL), NULL, NULL, NULL, NULL, APPLICATION_NAME, NULL };

	RegisterClassEx(&wc);
	RECT rc = { 0, 0, Width, Height };
	AdjustWindowRect(&rc, WS_OVERLAPPEDWINDOW, FALSE);

	// Create the application's window
	HWND hWnd = CreateWindow(APPLICATION_NAME, APPLICATION_NAME, WS_OVERLAPPEDWINDOW, 100, 100, rc.right - rc.left, rc.bottom - rc.top, NULL, NULL, wc.hInstance, NULL);

	//Engine
	CEngine l_engine = CEngine();
	
	//ActionManager
	CActionManager *l_actionManager = new CActionManager(hWnd);
	l_actionManager->LoadActions("actions.xml");
	l_engine.SetActionManager(l_actionManager);

	CMaterialManager *l_materialManager = new CMaterialManager();
	l_materialManager->Load("Levels.xml", "Defaults.xml");
	l_engine.SetMaterialManager(l_materialManager);

	// RenderManager
	CRenderManager *l_renderManager = new CRenderManager();
	l_renderManager->Init(hWnd, Width, Height);
	l_renderManager->SetModelMatrix(Mat44f().SetIdentity());
	l_renderManager->SetProjectionMatrix(45.0, (float)Width / (float)Height, 0.5f, 100.0f);
	l_renderManager->SetViewMatrix(Vect3f(10, 5, 7), Vect3f(0, 0, 0), Vect3f(0, 1, 0));
	l_engine.SetRenderManager(l_renderManager);

	//Camera
	CCameraController *l_cameraController = new CSphericalCameraController();
	l_engine.SetCameraController(l_cameraController);




	//ImGui setup
	ImGui_ImplDX11_Init(hWnd, l_engine.GetRenderManager().GetDevice(), l_engine.GetRenderManager().GetDeviceContext());


	ShowWindow(hWnd, SW_SHOWDEFAULT);
	UpdateWindow(hWnd);
	MSG msg;
	ZeroMemory(&msg, sizeof(msg));

	// Main Loop

	while (msg.message != WM_QUIT)
	{
		l_engine.GetActionManager().PreUpdate(s_WindowActive);
		l_engine.GetActionManager().PostUpdate();

		while (PeekMessage(&msg, NULL, 0U, 0U, PM_REMOVE))
		{

			ImGui_ImplDX11_WndProcHandler(hWnd, msg.message, msg.wParam, msg.lParam);

			bool fHandled = false;
			if ((msg.message >= WM_MOUSEFIRST && msg.message <= WM_MOUSELAST) || msg.message == WM_INPUT)
			{
				fHandled = l_engine.GetActionManager().HandleMouse(msg);
			}
			else if (msg.message >= WM_KEYFIRST && msg.message <= WM_KEYLAST)
			{
				fHandled = l_engine.GetActionManager().HandleKeyboard(msg);
			}

			if (!fHandled)
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}

			if (msg.message == WM_QUIT)
				break;
		}

		{

			// Main loop: A�adir aqu� el Update y Render de la aplicaci�n principal
			ImGui_ImplDX11_NewFrame();
			displayImguiInfo();

			l_engine.ProcessInputs();
			l_engine.Update();
			l_engine.Render();
		}
	}
	UnregisterClass(APPLICATION_NAME, wc.hInstance);

	// A�adir una llamada a la alicaci�n para finalizar/liberar memoria de todos sus datos
	delete l_renderManager;
	delete l_actionManager;
	delete l_cameraController;

	return 0;

#ifdef _DEBUG
	MemLeaks::MemoryEnd();
#endif;
}

